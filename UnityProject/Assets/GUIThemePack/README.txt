Thank you for purchasing this GUI package!

-- Usage --

Using these GUI images in your project is easy! All you need to do is:

1. Import purchased theme. Note that if you get any red erros, it may be because you don't have NGUI. Just remove the NGUI folder if this is the case.
3. Use the images to setup your game GUI!


-- More advanced usage --

You're free do to with this GUI package whatever you want (apart from re-selling or giving away)!
But please be cautious, and duplicate all files that you're planning to modify. You still can accidentally re-import packages and overwrite your changes!
If you plan on updating the package via the asset store mid-development, please be aware than changes may alter your project. Always back up!

You can find more matching GUI content here! https://www.assetstore.unity3d.com/en/#!/publisher/3365

-- Support --

If you have any questions regarding the the package it's self or if you have any suggestions of things I could add to it, by all means contact the creator by emailing earlygamesproductions@gmail.com. 