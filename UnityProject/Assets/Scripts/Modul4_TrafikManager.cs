﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class Modul4_TrafikManager : MonoBehaviour
{

    AudioSource _myAudioSource;

    public AudioClip snd_aferin;
    public AudioClip snd_tekrarDusun;

    public AudioClip snd_yesileBas;
    public AudioClip snd_kirmiziyaBas;
    public AudioClip snd_sariyaBas;

    public AudioClip snd_kirmiziDur;
    public AudioClip snd_sariBekle;
    public AudioClip snd_yesilGec;

    public AudioClip snd_IsiklarinAnlamlari;
    public AudioClip snd_trafikIsikAnlamlari;

    public GameObject[] traficLights;
    public Texture2D[] traficLightTextures;
    public Button[] traficLightButtons;
    public GameObject[] traficLightEffects;

    public AudioClip snd_karsidanKarsiya;
    public AudioClip snd_ornekBakalim;
    public AudioClip snd_kirmiziSoru;
    public AudioClip snd_sariSoru;
    public AudioClip snd_yesilSoru;
    public GameObject yesNoPanel;

    public Image fader;

    BADU_GSConnect googleConnectCntrl;
    string PlUserName = " ";
    string moduleName = " ";
    string QuestionName = " ";
    int answerCount = 1;
    float correctAnswerTime = 0;

    float fogDist = 100;

    private bool[] pressed = new bool[] { false, false, false };

    void Start()
    {
        foreach (Button g in traficLightButtons)
            g.gameObject.SetActive(false);

        string nameGet = PlayerPrefs.GetString("CurrentPlayerName");
        PlUserName = nameGet;
        googleConnectCntrl = GameObject.FindGameObjectWithTag("GoogleConnect").GetComponent<BADU_GSConnect>();

        fader.DOFade(0f, 1f);

        fader.raycastTarget = false;
        _myAudioSource = GetComponent<AudioSource>();
        StartCoroutine("traficIntro");
        yesNoPanel.transform.DOLocalMoveY(1000f, 1f, true);
    }

    public IEnumerator traficIntro()
    {
        fogDist = 100;
        _myAudioSource.PlayOneShot(snd_IsiklarinAnlamlari);

        while (_myAudioSource.isPlaying)
            yield return null;

        Camera.main.GetComponent<CameraMovementController>().BtoA();
        yield return new WaitForSeconds(1f);

        StartCoroutine("traficLightsSingle");
    }

    public IEnumerator traficLightsSingle()
    {
        fogDist = 20;
        LightsChange(1, 1, 1);
        LightsButtonChange(0, 0, 0, false);
        yield return new WaitForSeconds(3f);
        _myAudioSource.PlayOneShot(snd_trafikIsikAnlamlari);

        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(2f);

        LightsChange(0, 0, 0);

        tLpressed = false;
        LightsChange(1, 0, 0);
        _myAudioSource.PlayOneShot(snd_kirmiziyaBas);
        while (_myAudioSource.isPlaying)
            yield return null;
        traficLightButtons[0].gameObject.SetActive(true); 
        LightsButtonChange(1, 0, 0, false);
        while (!tLpressed) { yield return null; }
        traficLightEffects[0].SetActive(true);
        yield return new WaitForSeconds(5f);

        tLpressed = false;
        LightsChange(0, 1, 0);
        _myAudioSource.PlayOneShot(snd_sariyaBas);
        traficLightButtons[1].gameObject.SetActive(true);
        while (_myAudioSource.isPlaying)
            yield return null;
        LightsButtonChange(0, 1, 0, false);
        while (!tLpressed) { yield return null; }
        traficLightEffects[1].SetActive(true);

        yield return new WaitForSeconds(5f);

        tLpressed = false;
        LightsChange(0, 0, 1);
        _myAudioSource.PlayOneShot(snd_yesileBas);
        traficLightButtons[2].gameObject.SetActive(true);
        while (_myAudioSource.isPlaying)
            yield return null;
        LightsButtonChange(0, 0, 1, false);
        while (!tLpressed) { yield return null; }
        traficLightEffects[2].SetActive(true);

        yield return new WaitForSeconds(4f);
        _myAudioSource.PlayOneShot(snd_aferin);
        LightsChange(1, 0, 0);
        LightsButtonChange(0, 0, 0, true);

        Debug.Log("cam movement AtoB");
        Camera.main.GetComponent<CameraMovementController>().AtoB();
        fogDist = 100;

        StartCoroutine("TraficLightsPartB");
        yield break;
    }

    IEnumerator TraficLightsPartB()
    {
        yield return new WaitForSeconds(5f);

        _myAudioSource.PlayOneShot(snd_karsidanKarsiya);

        while (_myAudioSource.isPlaying)
            yield return null;

        _myAudioSource.PlayOneShot(snd_ornekBakalim);

        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1);
        Camera.main.GetComponent<CameraMovementController>().ToPos(2);
        yield return new WaitForSeconds(1);

        //question 1
        part2Pressed = false;
        LightsChange(1, 0, 0);
        _myAudioSource.PlayOneShot(snd_kirmiziSoru);

        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1f);
        yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);
        currCorrectAnswer = false;
        correctAnswerTime = Time.time;

        while (!part2Pressed) { yield return null; }
        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul4", "SoruKirmizi", 0, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(2f);
        //question 2
        part2Pressed = false;
        LightsChange(0, 1, 0);
        yield return new WaitForSeconds(1f);
        _myAudioSource.PlayOneShot(snd_sariSoru);

        while (_myAudioSource.isPlaying)
            yield return null;

        correctAnswerTime = Time.time;

        yield return new WaitForSeconds(1f);
        yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);
        currCorrectAnswer = false;
        while (!part2Pressed) { yield return null; }

        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul4", "SoruSari", 0, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(2f);
        //question 3
        part2Pressed = false;
        LightsChange(0, 0, 1);
        yield return new WaitForSeconds(1f);
        _myAudioSource.PlayOneShot(snd_sariSoru);

        while (_myAudioSource.isPlaying)
            yield return null;

        correctAnswerTime = Time.time;

        yield return new WaitForSeconds(1f);
        currCorrectAnswer = true;
        yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);
        while (!part2Pressed) { yield return null; }
        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul4", "SoruYesil", 0, Time.time - correctAnswerTime);

        //module finished
        fader.DOFade(1f, 1f);
        fader.raycastTarget = true;
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Modul5_otobus");

        yield break;
    }

    bool tLpressed = false;
    bool part2Pressed = false;
    bool currCorrectAnswer = false;

    public void part2AnswerReceived(bool isYes)
    {
        if (isYes == currCorrectAnswer)
        {
            //answer is correct so move up
            yesNoPanel.transform.DOLocalMoveY(1000f, 1f, true);
            part2Pressed = true;
            _myAudioSource.PlayOneShot(snd_aferin);
        }
        else
        {
            _myAudioSource.PlayOneShot(snd_tekrarDusun);
        }
    }

    void LightsChange(int red, int yellow, int green)
    {
        traficLights[0].GetComponent<Renderer>().material.mainTexture = traficLightTextures[red];
        traficLights[1].GetComponent<Renderer>().material.mainTexture = traficLightTextures[yellow];
        traficLights[2].GetComponent<Renderer>().material.mainTexture = traficLightTextures[green];
    }

    void LightsButtonChange(int red, int yellow, int green, bool hide)
    {
        traficLightButtons[0].image.raycastTarget = red > 0;
        traficLightButtons[0].gameObject.SetActive(!hide);
        traficLightButtons[1].image.raycastTarget = yellow > 0;
        traficLightButtons[1].gameObject.SetActive(!hide);
        traficLightButtons[2].image.raycastTarget = green > 0;
        traficLightButtons[2].gameObject.SetActive(!hide);
    }

    public void TrafficLightsClick(int lightId)
    {
        tLpressed = true;
        traficLightButtons[lightId - 1].gameObject.SetActive(false);
        switch (lightId)
        {
            case 1:
                {
                    _myAudioSource.PlayOneShot(snd_kirmiziDur);
                    break;
                }
            case 2:
                {
                    _myAudioSource.PlayOneShot(snd_sariBekle);
                    break;
                }
            case 3:
                {
                    _myAudioSource.PlayOneShot(snd_yesilGec);
                    break;
                }
        }

    }

    void Update()
    {
        if (fogDist != RenderSettings.fogEndDistance)
            RenderSettings.fogEndDistance = Mathf.Lerp(RenderSettings.fogEndDistance, fogDist, 0.1f);
    }
}
