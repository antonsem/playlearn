﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;


public class Modul1_ifadelerNew : MonoBehaviour
{

    public AudioClip[] sorular;
    public Button[] cevaplar;
    public Sprite[] ifadeler;
    public bool[] dogruCevap;
    public Sprite dogruCevapTik;

    public AudioClip snd_merhaba;
    public AudioClip snd_dogru;
    public AudioClip snd_yanlis;
    public AudioClip snd_alkis;

    public int soruID = 0;
    public List<int> currSoruIdList = new List<int>();
    public List<int> availableSoruIdList = new List<int>();
    private AudioSource _myAudioSource;

    public int cevapId = 0;

    bool skipQuestion = false;

    public bool isDebug = false;

    public Texture2D dogruCevapTex;
    public Texture2D yanlisCevapTex;

    public bool isModuleCompleted = false;
    public Text nameText;
    public GameObject[] victoryGos;
    public GameObject[] pictureHolderGos;
    public Button[] pictureHolderButtons;

    public Image fader;

    BADU_GSConnect googleConnectCntrl;
    string PlUserName = " ";
    string moduleName = " ";
    string QuestionName = " ";
    int answerCount = 1;
    float correctAnswerTime = 0;

    public CanvasGroup teacherCg;
    public Text teacherBubble;

    public bool isCevapGeldi = false;
    public bool DogruCevapGeldi = false;

    IEnumerator Start()
    {
        fader.DOFade(0f, 1f);
        fader.raycastTarget = false;
        _myAudioSource = GetComponentInChildren<AudioSource>();
        //merhaba
        _myAudioSource.PlayOneShot(snd_merhaba);

        nameText.text = "MERHABA   ";

        if (PlayerPrefs.HasKey("CurrentPlayerName"))
        {
            string nameGet = PlayerPrefs.GetString("CurrentPlayerName");
            PlUserName = nameGet;
            nameText.text = "MERHABA   " + nameGet;
        }

        //start stop to buffer camera victory
        for (int i = 0; i < victoryGos.Length; i++)
        {
            victoryGos[i].SetActive(true);
            victoryGos[i].SetActive(false);
        }

        yield return new WaitForSeconds(1f);
        GameObject temp = GameObject.FindGameObjectWithTag("GoogleConnect");
        if (temp) googleConnectCntrl = temp.GetComponent<BADU_GSConnect>();

        //reset the module
        isCevapGeldi = false;
        skipQuestion = false;
        isModuleCompleted = false;
        //		currSoruIdList.Clear ();

        StartCoroutine("TekrarSoruSor");
        yield break;
    }

    IEnumerator TekrarSoruSor()
    {
        //		case 0: kizgin
        //		case 1: korkmus
        //		case 2: mutlu
        //		case 3: saskin
        //		case 4: gururlu
        //		case 5: uzgun

#if true

        /*
		 * Şimdi bana korkmuş yüzü gösterebilir misin?
		1.Afraid Man versus Angry Man
		2.Afraid Man vs Proud Man
		3.Afraid Man vs Happy Man
		*/
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(6, 7, 999, 6, 1));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(6, 9, 999, 6, 1));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(6, 8, 999, 6, 1));

        /*
		Şimdi bana şaşırmış yüzü gösterebilir misin?
		4.Suprised Man vs Proud Man
		5.Suprised Man vs Happy Man
		6.Suprised Woman vs Happy Woman
		*/
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(14, 9, 999, 14, 3));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(14, 8, 999, 14, 3));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(13, 11, 999, 13, 3));

        /*
		Şimdi bana üzgün yüzü gösterebilir misin? (Stock fotolardan)
		7.Sad Boy vs Angry Boy
		8.Sad Boy vs Happy Boy
		9.Sad Girl vs Happy Girl
		10.Sad Girl vs Angry Girl
		*/

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(2, 0, 999, 2, 5));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(2, 1, 999, 2, 5));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(5, 3, 999, 5, 5));


        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(5, 4, 999, 5, 5));

        /*
		Şimdi bana gururlu yüzü gösterebilir misiniz?
		11.Proud Man vs Angry Man
		12.Proud Man vs Suprised Man
		13.Proud Man vs Happy Man
		14.Proud Woman vs Angry Woman
		*/

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(9, 7, 999, 9, 4));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(9, 14, 999, 9, 4));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(9, 8, 999, 9, 4));


        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(12, 10, 999, 12, 4));

        /*
		Şimdi bana kızgın yüzü gösterebilir misin?
		15.Angry Man vs Suprised Man
		16.Angry Man vs Happy Man
		17.Angry Woman vs Happy Woman
		18.Angry Woman vs Proud Woman
		*/

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(7, 14, 999, 7, 0));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(7, 8, 999, 7, 0));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(10, 11, 999, 10, 0));


        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(10, 12, 999, 10, 0));


        /*
		Şimdi bana mutlu yüzü gösterebilir misin?

		19.Happy Man vs Angry Man
		20.HappyWoman vs Suprised Woman
		21.Happy Girl vs Sad Girl
		22.Happy Girl vs Angry Girl
		23.Happy boy vs Sad Boy
		24.Happy Boy vs Angry Boy
		*/

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(8, 7, 999, 8, 2));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(11, 13, 999, 11, 2));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(4, 5, 999, 4, 2));


        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(4, 3, 999, 4, 2));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(1, 2, 999, 1, 2));


        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(1, 0, 999, 1, 2));


        /*
		Şimdi bana mutlu yüzü gösterebilir misin?

		25.Happy Man vs Angry Man vs Proud Man
		26.Happy Woman vs Angry Woman vs Suprised Woman
		27.Happy Boy vs Angry Boy vs Sad Boy
		28.Happy Girl vs Angry Girl vs Sad girl
		*/

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(8, 7, 9, 8, 2));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(11, 13, 10, 11, 2));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(1, 0, 2, 1, 2));


        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(4, 3, 5, 4, 2));

        /*
		Şimdi bana kızgın yüzü gösterebilir misin?
		29.Happy Boy vs Angry Boy vs Sad Boy
		30.Happy girl vs Angry Girl vs Sad Girl
		31. Angry Woman vs Happy Woman vs Suprised Woman
		32. Angry Man vs Happy Man vs Suprised Man
		*/

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(0, 1, 2, 0, 0));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(3, 4, 5, 3, 0));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(10, 11, 13, 10, 0));


        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(7, 8, 14, 7, 0));


        /*
		Şimdi bana gururlu yüzü gösterebilir misiniz?
		33.Proud Man vs Angry Man vs Happy Man
		34.Proud Woman vs Angry Woman vs Suprised Woman
		*/

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(9, 8, 7, 9, 4));


        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(12, 10, 13, 12, 4));

        /*
		Şimdi bana şaşırmış yüzü gösterebilir misin?
		35.Suprised Man vs Angry Man vs Proud Man
		36.Suprised Woman vs Happy Woman vs Proud Woman
		*/


        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(14, 7, 9, 14, 3));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(13, 11, 10, 13, 3));

        /*
		 * Şimdi bana korkmuş yüzü gösterebilir misin?
		37.Afraid Man vs Angry Man vs Happy Man
		38.Afraid Man vs Suprised Man vs Proud Man
		39.Afraid Woman vs Happy Woman vs Proud Woman
		40.Afraid Woman vs Suprised Woman vs Proud Woman
		*/
        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(6, 7, 8, 6, 1));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(6, 9, 14, 6, 1));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(15, 11, 12, 15, 1));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(15, 13, 12, 15, 1));

        /*
		Şimdi bana üzgün yüzü gösterebilir misin? (Stock fotolardan)
		41.Sad girl vs Happy Girl vs Angry Girl
		42.Sad boy vs Happy Boy vs Angry Boy
		*/

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(5, 4, 3, 5, 5));

        yield return new WaitForSeconds(1f);
        yield return StartCoroutine(SiraliSoru(2, 1, 0, 2, 5));



        /*
		boy_angry;//0
		boy_happy; //1
		boy_sad;//2

		girl_angry;//3
		girl_happy;//4
		girl_sad;//5

		man_afraid;//6
		man_angry;//7
		 man_happy;//8
		man_proud;//9
		man_surprised; //14
		
		woman_angryy;//10
		woman_happy;//11
		woman_proud;//12
		woman_surprised;//13
		woman_afraid;//15
		*/
#endif

        //finished the questions
        //!set_B && 
        if (PlayerPrefs.HasKey(PlUserName))
        {
            int currentModulesFinished = PlayerPrefs.GetInt(PlUserName);

            string levelCompletedCode = currentModulesFinished.ToString();
            Debug.Log(levelCompletedCode + "  " + levelCompletedCode[2]);

            if (levelCompletedCode[1] != levelCompletedCode[0])
            {
                currentModulesFinished = currentModulesFinished + 10000;///first module finished. Module counts from left
				PlayerPrefs.SetInt(PlUserName, currentModulesFinished);
                PlayerPrefs.Save();
            }
        }

        yield return new WaitForSeconds(0.1f);
        //start camera victory
        for (int i = 0; i < victoryGos.Length; i++)
        {
            if (i == 2 || i == 3)
            {//girl
                victoryGos[i].SetActive(true);
                victoryGos[i].SetActive(true);
            }
            else
                victoryGos[i].SetActive(true);
        }
        _myAudioSource.PlayOneShot(snd_alkis);

        yield return new WaitForSeconds(7f);
        fader.DOFade(1f, 1f);
        fader.raycastTarget = true;
        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene("MainMenu");
        StopModule();
        yield break;
    }

    public Sprite[] YeniIfadeler;

    IEnumerator SiraliSoru(int SecenekA, int SecenekB, int SecenekC, int rightAnswerId, int soruCesidiId)
    {
        int siralama = 0;
        //first one is always the right one
        if (SecenekC == 999)
        {//999 is null
            pictureHolderGos[2].SetActive(false);
            siralama = Random.Range(0, 2);
        }
        else
        {
            pictureHolderGos[2].SetActive(true);
            siralama = Random.Range(0, 4);
        }

        switch (siralama)
        {
            case 0:
                {
                    pictureHolderButtons[0].image.sprite = YeniIfadeler[SecenekA];
                    pictureHolderButtons[1].image.sprite = YeniIfadeler[SecenekB];
                    if (SecenekC != 999)
                    {
                        pictureHolderButtons[2].image.sprite = YeniIfadeler[SecenekC];
                        pictureHolderButtons[2].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = false;
                    }
                    pictureHolderButtons[0].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = true;
                    pictureHolderButtons[1].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = false;
                    break;
                }
            case 1:
                {
                    pictureHolderButtons[1].image.sprite = YeniIfadeler[SecenekA];
                    pictureHolderButtons[0].image.sprite = YeniIfadeler[SecenekB];
                    if (SecenekC != 999)
                    {
                        pictureHolderButtons[2].image.sprite = YeniIfadeler[SecenekC];
                        pictureHolderButtons[2].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = false;
                    }
                    pictureHolderButtons[1].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = true;
                    pictureHolderButtons[0].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = false;

                    break;
                }
            case 2:
                {
                    pictureHolderButtons[2].image.sprite = YeniIfadeler[SecenekA];
                    pictureHolderButtons[1].image.sprite = YeniIfadeler[SecenekB];
                    pictureHolderButtons[0].image.sprite = YeniIfadeler[SecenekC];
                    pictureHolderButtons[2].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = true;
                    pictureHolderButtons[1].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = false;
                    pictureHolderButtons[0].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = false;

                    break;
                }
            case 3:
                {
                    pictureHolderButtons[2].image.sprite = YeniIfadeler[SecenekA];
                    pictureHolderButtons[0].image.sprite = YeniIfadeler[SecenekB];
                    pictureHolderButtons[1].image.sprite = YeniIfadeler[SecenekC];
                    pictureHolderButtons[2].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = true;
                    pictureHolderButtons[0].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = false;
                    pictureHolderButtons[1].GetComponent<modul1_ifadelerCevaplar>().isTrueAnswer = false;

                    break;
                }
        }
        pictureHolderButtons[0].name = pictureHolderButtons[0].image.sprite.name;
        pictureHolderButtons[1].name = pictureHolderButtons[1].image.sprite.name;
        if (SecenekC != 999)
        {
            pictureHolderButtons[2].name = pictureHolderButtons[2].image.sprite.name;

        }


        pictureHolderButtons[0].interactable = true;
        pictureHolderButtons[1].interactable = true;
        if (SecenekC != 999)
        {
            pictureHolderButtons[2].interactable = true;
        }


        yield return StartCoroutine(soruSor(soruCesidiId));
        yield break;

    }

    IEnumerator soruSor(int currentSoruId)
    {
        //soruyu sor
        isCevapGeldi = false;
        _myAudioSource.PlayOneShot(sorular[currentSoruId]);

        while (_myAudioSource.isPlaying)
        {
            yield return null;
        }

        correctAnswerTime = Time.time;
        while (true)
        {
            //wait for answer
            while (!isCevapGeldi)
            {
                if (isDebug) Debug.Log("cevap bekliyor ");
                yield return null;
            }

            //answer arrived check true or false
            if (DogruCevapGeldi || skipQuestion)
            {
                //true answer
                if (!skipQuestion) { _myAudioSource.PlayOneShot(snd_dogru); }

                availableSoruIdList.Remove(cevapId);

                //upload data and reset counters	
                if (googleConnectCntrl) googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul1", "Soru No : " + currentSoruId, answerCount, Time.time - correctAnswerTime);
                correctAnswerTime = 0;
                answerCount = 1;

                yield break;
            }
            else
            {
                //wrong answer
                answerCount++;
                _myAudioSource.PlayOneShot(snd_yanlis);
                isCevapGeldi = false;
                yield return null;
            }
        }
    }

    public void SkipQuestion()
    {
        isCevapGeldi = skipQuestion = true;
    }

    public void StopModule()
    {
        isCevapGeldi = skipQuestion = true;
        StopAllCoroutines();
    }


}
