﻿using UnityEngine;

public class LookAtCam : MonoBehaviour
{
    Transform cam;

    void Start()
    {
        cam = Camera.main.transform;
    }

    void Update()
    {
        transform.LookAt(cam.position);
    }
}
