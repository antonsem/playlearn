﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class CameraMovementController : MonoBehaviour
{
    public Transform CamTrnsA;
    public Transform CamTrnsB;
    public Transform[] camPositions;
    Transform _myTrns;

    void Start()
    {
        _myTrns = transform;
    }

    public void AtoB()
    {
        _myTrns.position = CamTrnsA.position;
        _myTrns.rotation = CamTrnsA.rotation;

        StartCoroutine(GoAtoBView());
    }

    public void BtoA()
    {
        _myTrns.position = CamTrnsB.position;
        _myTrns.rotation = CamTrnsB.rotation;
        StartCoroutine(GoBtoAView());
    }

    public void ToPos(int index)
    {
        if(camPositions == null || camPositions.Length <= index)
        {
            Debug.LogError("Camera index is out of range!");
            return;
        }

        StartCoroutine(GoToPos(camPositions[index]));
    }

    IEnumerator GoToPos(Transform t)
    {
        yield return new WaitForSeconds(0.2f);
        _myTrns.DOMove(t.position, 2f, false);
        _myTrns.DORotate(t.eulerAngles, 2f, RotateMode.Fast);
        yield break;
    }

    IEnumerator GoAtoBView()
    {
        yield return new WaitForSeconds(0.2f);
        _myTrns.DOMove(CamTrnsB.position, 2f, false);
        _myTrns.DORotate(CamTrnsB.eulerAngles, 2f, RotateMode.Fast);
        yield break;
    }

    IEnumerator GoBtoAView()
    {
        yield return new WaitForSeconds(0.2f);
        _myTrns.DOMove(CamTrnsA.position, 2f, false);
        _myTrns.DORotate(CamTrnsA.eulerAngles, 2f, RotateMode.Fast);
        yield break;
    }
}
