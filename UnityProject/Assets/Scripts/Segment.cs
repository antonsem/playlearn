﻿using UnityEngine;
using System;

public enum QuizType
{
    HappyMan = 0,
    AngryMan = 1,
    SadMan = 2,
    SurprisedMan = 3,
    HappyWoman = 4,
    AngryWoman = 5,
    SadWoman = 6,
    SurprisedWoman = 7,
    None
}

[Serializable]
public class Segment
{
    public AudioClip[] teacherTalk;
    public QuizType quizType = QuizType.None;
    public int pictureCount = 2;
    public string movieName = "";
    //public MovieTexture movie;
}
