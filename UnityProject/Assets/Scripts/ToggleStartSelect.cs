﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ToggleStartSelect : MonoBehaviour {

	ToggleGroup _myToggle;
	public Toggle girl;
	public Toggle boy;

	// Use this for initialization
	void Start () {
		_myToggle = GetComponent<ToggleGroup> ();

		if (PlayerPrefs.HasKey ("SelectedCharacter")) {
			if (PlayerPrefs.GetInt ("SelectedCharacter") == 0) {
				_myToggle.NotifyToggleOn (girl);
				girl.isOn = true;
			}else{
				_myToggle.NotifyToggleOn (boy);
				boy.isOn = true;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
