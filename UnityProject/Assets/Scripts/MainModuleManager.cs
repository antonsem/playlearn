﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class MainModuleManager : MonoBehaviour
{
    public string playerName = "";
    public InputField nameInputField;
    public AudioClip menuMusic;

    public AudioClip snd_merhabaBenSevgi;
    public AudioClip snd_merhabaBenCan;

    AudioSource _myAudioSource;
    public AudioSource _myMusicAudioSource;

    public GameObject GoogleConnectCntrl;

    public GameObject[] modulePlayedChecks;
    string levelCompletedCode = "100000";//the first digit no : 1 is for formatting

    public GameObject TB_BenSevgi;
    public GameObject TB_BenCan;
    public CanvasGroup mainButtonsCanvasGroup;
    public Animator anim;
    private List<AudioClip> audioQueue = new List<AudioClip>();
    public List<InspectorDictionary> audioDict = new List<InspectorDictionary>();

    public void UpdatePlayerName(InputField playerInput)
    {
        if (playerInput.text != "")
        {
            playerName = playerInput.text;
            PlayerPrefs.SetString("CurrentPlayerName", playerInput.text);

            if (!PlayerPrefs.HasKey(playerName))
            {
                PlayerPrefs.SetInt(playerName, 100000);
            }

            PlayerPrefs.Save();
        }
        else
        {
            PlayerPrefs.DeleteKey("CurrentPlayerName");
        }

    }

    void Start()
    {
        SetButtonsVisible(false);
#if UNITY_EDITOR
        StaticEventsManager.GameStartedAlready = true;
#endif
        if (PlayerPrefs.HasKey("CurrentPlayerName"))
        {
            playerName = PlayerPrefs.GetString("CurrentPlayerName");
            nameInputField.text = playerName;
        }
        _myAudioSource = GetComponent<AudioSource>();
        _myMusicAudioSource.PlayOneShot(menuMusic);

        if (!PlayerPrefs.HasKey("SelectedCharacter"))
            PlayerPrefs.SetInt("SelectedCharacter", 0);

        if (GameObject.FindGameObjectWithTag("GoogleConnect") == null)
            Instantiate(GoogleConnectCntrl, Vector3.zero, Quaternion.identity);

        if (PlayerPrefs.HasKey(playerName))
        {
            levelCompletedCode = PlayerPrefs.GetInt(playerName).ToString("000000");

            for (int i = 1; i < levelCompletedCode.Length; i++)
                modulePlayedChecks[i - 1].SetActive(levelCompletedCode[i] != '0');
        }

        if (!StaticEventsManager.GameStartedAlready)
        {
            StaticEventsManager.GameStartedAlready = true;
            anim.SetTrigger("intro");
        }
        else
        {
            SetButtonsVisible(true);
        }
    }

    public void SetButtonsVisible(bool state)
    {
        if (state)
        {
            mainButtonsCanvasGroup.DOFade(1f, 0.5f);
            mainButtonsCanvasGroup.interactable = true;
        }
        else
        {
            mainButtonsCanvasGroup.alpha = 0f;
            mainButtonsCanvasGroup.interactable = false;
        }
    }

    public void StartFirstModule()
    {
        _myAudioSource.Stop();
        SceneManager.LoadScene("Modul1");
    }

    public void StartSecondModule()
    {
        _myAudioSource.Stop();
        SceneManager.LoadScene("Modul2");
    }

    public void StartThirdModule()
    {
        _myAudioSource.Stop();
        SceneManager.LoadScene("Modul3_IndirekKonusma");
    }

    public void StartFourthtModule()
    {
        _myAudioSource.Stop();
        SceneManager.LoadScene("Modul4_Trafiklambalari");
    }

    public void StartFifthModule()
    {
        _myAudioSource.Stop();
        SceneManager.LoadScene("Modul6_Halkalar");

    }

    public void FirstModuleCompleted()
    {
        _myAudioSource.Stop();

    }

    public void SelectCharacter(bool isGirlSelected)
    {
        if (isGirlSelected)
        {
            Debug.Log("isGirlSelected");
            PlayerPrefs.SetInt("SelectedCharacter", 0);//girl selected
        }
        else
        {
            Debug.Log("isBoySelected");
            PlayerPrefs.SetInt("SelectedCharacter", 1);//boy selected
        }
    }

    public void PlaySoundWithQueue(string clipName)
    {
        InspectorDictionary clip = audioDict.Find(ac => ac._name == clipName);
        if (clip != null)
            audioQueue.Add(clip._clip);
        else
            Debug.Log("No audio clip with name " + clipName);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha5))
            SceneManager.LoadScene("Modul5_otobus");

        if (audioQueue.Count > 0 && !_myAudioSource.isPlaying)
        {
            _myAudioSource.PlayOneShot(audioQueue[0]);
            audioQueue.Remove(audioQueue[0]);
        }
    }
}
