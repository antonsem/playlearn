﻿using UnityEngine;
using System;

[Serializable]
public class InspectorDictionary
{
    public string _name;
    public AudioClip _clip;

}
