﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;


public class Modul1_ifadelerManager : MonoBehaviour
{
    public AudioClip[] sorular;
    public Button[] cevaplar;
    public Sprite[] ifadeler;
    public bool[] dogruCevap;
    public Sprite dogruCevapTik;

    public AudioClip snd_merhaba;
    public AudioClip snd_dogru;
    public AudioClip snd_yanlis;
    public AudioClip snd_alkis;

    public int soruID = 0;
    public List<int> currSoruIdList = new List<int>();
    public List<int> availableSoruIdList = new List<int>();
    private AudioSource _myAudioSource;

    public bool isCevapGeldi = false;
    public int cevapId = 0;

    bool skipQuestion = false;

    public bool isDebug = false;

    public Texture2D dogruCevapTex;
    public Texture2D yanlisCevapTex;

    public bool isModuleCompleted = false;
    public Text nameText;
    public GameObject[] victoryGos;

    public Image fader;

    BADU_GSConnect googleConnectCntrl;
    string PlUserName = " ";
    string moduleName = " ";
    string QuestionName = " ";
    int answerCount = 1;
    float correctAnswerTime = 0;

    public CanvasGroup teacherCg;
    public Text teacherBubble;

    IEnumerator Start()
    {
        fader.DOFade(0f, 1f);
        fader.raycastTarget = false;
        _myAudioSource = GetComponentInChildren<AudioSource>();
        //merhaba
        _myAudioSource.PlayOneShot(snd_merhaba);


        //put pictures on buttons
        for (int i = 0; i < cevaplar.Length; i++)
        {
            cevaplar[i].image.sprite = ifadeler[i];
            cevaplar[i].GetComponentInChildren<modul1_ifadelerCevaplar>()._myCevapId = i;
            cevaplar[i].GetComponentInChildren<CanvasGroup>().alpha = 0f;
            cevaplar[i].enabled = true;
            availableSoruIdList.Add(i);
        }
        nameText.text = "MERHABA   ";

        if (PlayerPrefs.HasKey("CurrentPlayerName"))
        {
            string nameGet = PlayerPrefs.GetString("CurrentPlayerName");
            PlUserName = nameGet;
            nameText.text = "MERHABA   " + nameGet;
        }

        //start stop to buffer camera victory
        for (int i = 0; i < victoryGos.Length; i++)
        {
            victoryGos[i].SetActive(true);
            victoryGos[i].SetActive(false);
        }

        yield return new WaitForSeconds(2f);
        googleConnectCntrl = GameObject.FindGameObjectWithTag("GoogleConnect").GetComponent<BADU_GSConnect>();

        //reset the module
        isCevapGeldi = false;
        skipQuestion = false;
        isModuleCompleted = false;
        //		currSoruIdList.Clear ();

        StartCoroutine("TekrarSoruSor");
        yield break;
    }

    IEnumerator soruSor()
    {
        if (availableSoruIdList.Count > 0)
        {
            soruID = Random.Range(0, availableSoruIdList.Count);
        }
        else
        {
            isModuleCompleted = true;
            //record this module finished for player

            //!set_B && 
            if (PlayerPrefs.HasKey(PlUserName))
            {
                int currentModulesFinished = PlayerPrefs.GetInt(PlUserName);

                string levelCompletedCode = currentModulesFinished.ToString();
                Debug.Log(levelCompletedCode + "  " + levelCompletedCode[2]);

                if (levelCompletedCode[1] != levelCompletedCode[0])
                {
                    currentModulesFinished = currentModulesFinished + 10000;///first module finished. Module counts from left
					PlayerPrefs.SetInt(PlUserName, currentModulesFinished);
                    PlayerPrefs.Save();
                }
            }

            yield return new WaitForSeconds(0.1f);
            //start camera victory
            for (int i = 0; i < victoryGos.Length; i++)
            {
                if (i == 2 || i == 3)
                {
                    victoryGos[i].SetActive(true);
                    victoryGos[i].SetActive(true);
                }
                else
                    victoryGos[i].SetActive(true);
            }

            _myAudioSource.PlayOneShot(snd_alkis);

            yield return new WaitForSeconds(12f);
            fader.DOFade(1f, 1f);
            fader.raycastTarget = true;
            yield return new WaitForSeconds(1f);

            SceneManager.LoadScene("MainMenu");
            StopModule();
            yield break;
        }
        //be sure the soruId is different than previous 3
        //		while (currSoruIdList.Contains(soruID)) {
        //			soruID = Random.Range (0, sorular.Length);
        //			if(isDebug) Debug.Log ("yeni soruid hesaplanıyor ");
        //			yield return null;
        //		}

        //		currSoruIdList.Add (soruID);

        //3 question track keep
        //		if (currSoruIdList.Count > 3) {
        //			//keep only 3 entries
        //			if(isDebug)Debug.Log ("son soruid listeden siliniyor " + currSoruIdList[3]);
        //			currSoruIdList.RemoveAt (0);
        //		}

        //Clear all question
        //		if (currSoruIdList.Count > 6) {
        //			//keep only 3 entries
        //			if(isDebug)Debug.Log ("tum son sorular silinip bastan baslaniyor ");
        //
        //			currSoruIdList.Clear ();
        //		}

        if (isDebug) Debug.Log("soru basladı " + availableSoruIdList[soruID]);
        teacherCg.blocksRaycasts = true;
        teacherCg.DOFade(1f, 0.5f);

        switch (availableSoruIdList[soruID])
        {
            case 0:
                {
                    teacherBubble.text = "Şimdi, KIZGIN yüzü gösterebilirmisin? ";
                    break;
                }
            case 1:
                {
                    teacherBubble.text = "Şimdi, KORKMUŞ yüzü gösterebilirmisin? ";
                    break;
                }
            case 2:
                {
                    teacherBubble.text = "Şimdi bana, MUTLU yüzü gösterebilirmisin? ";
                    break;
                }
            case 3:
                {
                    teacherBubble.text = "Şimdi, ŞAŞKIN yüzü gösterebilirmisin? ";
                    break;
                }
            case 4:
                {
                    teacherBubble.text = " Şimdi bana, GURURLU yüzü gösterebilirmisin?";
                    break;
                }
            case 5:
                {
                    teacherBubble.text = " Şimdi, ÜZGÜN yüzü gösterebilirmisin?";
                    break;
                }

        }

        //set the true answer
        for (int i = 0; i < dogruCevap.Length; i++)
        {
            dogruCevap[i] = false;
        }
        dogruCevap[availableSoruIdList[soruID]] = true;


        //soruyu sor
        isCevapGeldi = false;
        _myAudioSource.PlayOneShot(sorular[availableSoruIdList[soruID]]);

        while (_myAudioSource.isPlaying)
        {
            yield return null;
        }

        yield return new WaitForSeconds(2f);
        teacherCg.DOFade(0f, 0.2f);
        teacherCg.blocksRaycasts = false;
        correctAnswerTime = Time.time;

        while (true)
        {

            //wait for answer
            while (!isCevapGeldi)
            {
                if (isDebug) Debug.Log("cevap bekliyor ");
                yield return null;
            }

            //answer arrived check true or false
            if (dogruCevap[cevapId] == true || skipQuestion)
            {
                //true answer
                if (!skipQuestion) { _myAudioSource.PlayOneShot(snd_dogru); }
                cevaplar[cevapId].interactable = false;
                cevaplar[cevapId].image.raycastTarget = false;
                cevaplar[cevapId].image.sprite = dogruCevapTik;

                availableSoruIdList.Remove(cevapId);

                //upload data and reset counters	
                string moduleNa = "Modul1";
                googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul1", cevaplar[cevapId].name, answerCount, Time.time - correctAnswerTime);
                correctAnswerTime = 0;
                answerCount = 1;
                //				string PlUserName = " ";
                //				string moduleName = " ";
                //				string QuestionName = " ";
                //				int answerCount = 0;
                //				float correctAnswerTime = 0;

                yield break;
            }
            else
            {
                //wrong answer
                answerCount++;
                _myAudioSource.PlayOneShot(snd_yanlis);
                cevaplar[cevapId].GetComponentInChildren<CanvasGroup>().DOFade(1f, 0.2f).SetDelay(0f);
                //yield return new WaitForSeconds(0.1f);
                cevaplar[cevapId].GetComponentInChildren<CanvasGroup>().DOFade(0f, 0.2f).SetDelay(1f);
                while (_myAudioSource.isPlaying)
                {
                    yield return null;
                }
                //turn to waiting
                //cevaplar[cevapId].GetComponentInChildren<RawImage>().enabled = false;
                isCevapGeldi = false;
                yield return null;
            }
        }
    }

    IEnumerator TekrarSoruSor()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            if (isDebug) Debug.Log("soru baslıyor");
            skipQuestion = false;
            yield return StartCoroutine("soruSor");
            yield return null;
        }
    }

    public void SkipQuestion()
    {
        isCevapGeldi = skipQuestion = true;
    }

    public void StopModule()
    {
        isCevapGeldi = skipQuestion = true;
        StopAllCoroutines();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            availableSoruIdList.Clear();
    }
}
