﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class Modul_1 : MonoBehaviour
{

    public Segment[] segs;
    public AudioSource source;
    public RawImage movieTexture;
    public GameObject[] quizButtons;
    public Image quizPanel;
    public GameObject congratsPanel;

    public AudioClip aferin;
    public AudioClip tekrarDusun;
    public AudioClip mutluYuzuGoster;
    public AudioClip kizginYuzuGoster;
    public AudioClip uzgunYuzuGoster;
    public AudioClip sasirmisYuzuGoster;
    public AudioClip applause;
    public AudioClip music;

    public Sprite[] happyMan;
    public Sprite[] angryMan;
    public Sprite[] sadMan;
    public Sprite[] surprisedMan;
    public Sprite[] happyWoman;
    public Sprite[] angryWoman;
    public Sprite[] sadWoman;
    public Sprite[] surprisedWoman;

    public ParticleSystem[] congratsParticles;
    public AudioClip winSound;
    public AudioClip tryAgainSound;

    public GameObject[] winObjects;

    private bool answered = false;
    private int trueAnswer = 0;
    private Dictionary<QuizType, AudioClip> questionDict;
    private Dictionary<QuizType, Sprite[]> imageDict;

    #region Analitycs
    BADU_GSConnect googleConnectCntrl;
    string PlUserName = " ";
    string moduleName = " ";
    string QuestionName = " ";
    int answerCount = 1;
    float correctAnswerTime = 0;
    int buttonPressCount = 0;
    #endregion

    void Start()
    {
        GameObject temp = GameObject.FindGameObjectWithTag("GoogleConnect");
        if (temp) googleConnectCntrl = temp.GetComponent<BADU_GSConnect>();

        questionDict = new Dictionary<QuizType, AudioClip>()
        { { QuizType.HappyMan, mutluYuzuGoster }, { QuizType.AngryMan, kizginYuzuGoster},
            { QuizType.SadMan, uzgunYuzuGoster }, { QuizType.SurprisedMan, sasirmisYuzuGoster },
        { QuizType.HappyWoman, mutluYuzuGoster }, { QuizType.AngryWoman, kizginYuzuGoster},
            { QuizType.SadWoman, uzgunYuzuGoster }, { QuizType.SurprisedWoman, sasirmisYuzuGoster }};

        imageDict = new Dictionary<QuizType, Sprite[]>()
        { { QuizType.HappyMan, happyMan }, { QuizType.AngryMan, angryMan},
            { QuizType.SadMan, sadMan }, { QuizType.SurprisedMan, surprisedMan },
        { QuizType.HappyWoman, happyWoman }, { QuizType.AngryWoman, angryWoman},
            { QuizType.SadWoman, sadWoman }, { QuizType.SurprisedWoman, surprisedWoman }};

        StartCoroutine(Execute());
    }

    private IEnumerator Execute()
    {
        foreach (Segment s in segs)
        {
            if (s.teacherTalk != null && s.teacherTalk.Length > 0)
                yield return StartCoroutine(PlayAudio(s.teacherTalk));
            if (!string.IsNullOrEmpty(s.movieName))
            {
                Handheld.PlayFullScreenMovie(s.movieName + ".mp4", Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFit);
                yield return new WaitForSeconds(1);
            }
            if (s.quizType != QuizType.None)
                yield return StartCoroutine(StartQuiz(s.quizType, s.pictureCount));
        }
        StartCoroutine(EndGame());
    }

    private IEnumerator PlayAudio(AudioClip[] clips)
    {
        foreach (AudioClip c in clips)
        {
            source.PlayOneShot(c);
            while (source.isPlaying) yield return null;
        }
    }

    private IEnumerator PlayAudio(AudioClip clip)
    {
        source.PlayOneShot(clip);
        while (source.isPlaying) yield return null;
    }

    private IEnumerator StartQuiz(QuizType type, int picCount)
    {
        while (quizPanel.color.a < 0.95f)
            quizPanel.color = Color.Lerp(quizPanel.color, Color.white, 0.1f);
        quizPanel.color = Color.white;

        for (int i = 0; i < picCount; i++)
            quizButtons[i].SetActive(true);

        List<Sprite> tempArr = new List<Sprite>();
        List<Sprite> tempWrong = new List<Sprite>();

        foreach (Sprite s in GetTempArray(type))
            tempArr.Add(s);

        foreach (KeyValuePair<QuizType, Sprite[]> item in imageDict)
            if (((int)type >= 4 && (int)item.Key >= 4 || ((int)type < 4 && (int)item.Key < 4))
                && item.Key != type && Mathf.Abs((int)item.Key - (int)type) != 4)
                foreach (Sprite s in item.Value)
                    tempWrong.Add(s);

        int currentSoruId = 0;

        while (tempArr.Count > 0)
        {
            answered = false;
            answerCount = 1;
            Sprite tempImg = tempArr[Random.Range(0, tempArr.Count)];
            trueAnswer = Random.Range(0, picCount);
            int[] usedIndex = new int[tempWrong.Count];
            int wrongIndex = Random.Range(0, tempWrong.Count);
            int failsafe = 0;
            for (int i = 0; i < quizButtons.Length; i++)
            {
                while (usedIndex[wrongIndex] != 0 && failsafe < 200)
                {
                    wrongIndex = Random.Range(0, tempWrong.Count);
                    failsafe++;
                }

                if (failsafe >= 200)
                    Debug.LogError("failsafe mode during picture selecting was activated!");

                usedIndex[wrongIndex] = 1;

                if (i == trueAnswer)
                    quizButtons[i].GetComponent<Image>().sprite = tempImg;
                else
                    quizButtons[i].GetComponent<Image>().sprite = tempWrong[wrongIndex];
            }
            yield return StartCoroutine(PlayAudio(questionDict[type]));
            correctAnswerTime = Time.deltaTime;
            while (!answered || source.isPlaying) yield return null;
            yield return StartCoroutine(PlayAudio(aferin));

            if (googleConnectCntrl) googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul1", "Soru No : " + (int)type + "." + picCount + "." + currentSoruId, answerCount, Time.time - correctAnswerTime);
            currentSoruId++;
            tempArr.Remove(tempImg);
        }

        while (quizPanel.color.a > 0.05f)
            quizPanel.color = Color.Lerp(quizPanel.color, Color.clear, 0.1f);
        quizPanel.color = Color.clear;

        foreach (GameObject g in quizButtons)
            g.SetActive(false);
    }

    public void Answer(int i)
    {
        if (source.isPlaying)
            return;
        if (i != trueAnswer)
        {
            if (tryAgainSound != null)
                source.PlayOneShot(tryAgainSound);
            StartCoroutine(PlayAudio(tekrarDusun));
            answerCount++;
            return;
        }
        if (winSound != null)
            source.PlayOneShot(winSound);
        StartCoroutine(ShowStars());
        answered = true;
    }

    IEnumerator ShowStars()
    {
        if (congratsParticles != null && congratsParticles.Length > 0)
            congratsParticles[Random.Range(0, congratsParticles.Length)].Play();
        congratsPanel.transform.SetAsLastSibling();
        yield return new WaitForSeconds(2);
        congratsPanel.transform.SetAsFirstSibling();
    }

    private Sprite[] GetTempArray(QuizType type)
    {
        switch (type)
        {
            case QuizType.HappyMan:
                return happyMan;
            case QuizType.AngryMan:
                return angryMan;
            case QuizType.SadMan:
                return sadMan;
            case QuizType.SurprisedMan:
                return surprisedMan;
            case QuizType.HappyWoman:
                return happyWoman;
            case QuizType.AngryWoman:
                return angryWoman;
            case QuizType.SadWoman:
                return sadWoman;
            case QuizType.SurprisedWoman:
                return surprisedWoman;
            default:
                {
                    Debug.LogError("Wrong quiz type!");
                    return null;
                }
        }
    }

    IEnumerator EndGame()
    {
        foreach (GameObject g in winObjects)
            g.SetActive(true);

        source.PlayOneShot(applause);
        source.PlayOneShot(music);

        yield return new WaitForSeconds(10);

        if (PlayerPrefs.HasKey(PlUserName))
        {
            int currentModulesFinished = PlayerPrefs.GetInt(PlUserName);

            string levelCompletedCode = currentModulesFinished.ToString();
            Debug.Log(levelCompletedCode + "  " + levelCompletedCode[2]);

            if (levelCompletedCode[1] != levelCompletedCode[0])
            {
                currentModulesFinished = currentModulesFinished + 10000;///first module finished. Module counts from left
				PlayerPrefs.SetInt(PlUserName, currentModulesFinished);
                PlayerPrefs.Save();
            }
        }

        SceneManager.LoadScene("MainMenu");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            source.Stop();
    }
}
