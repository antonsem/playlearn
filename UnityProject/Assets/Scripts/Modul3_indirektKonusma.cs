﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;


public class Modul3_indirektKonusma : MonoBehaviour
{
    public AudioClip bazenDirekSoylemeyiz;
    public AudioClip ornekBakalim;
    public AudioClip baskaOrnek;
    public AudioClip benzerOrnek;
    public AudioClip beraberBaskaOrnekBakalim;
    public AudioClip merhabaSevgi;
    public AudioClip merhabaCan;
    public AudioClip annemKekYapti;
    public AudioClip kekYemeyeCagirdiMi;
    public AudioClip tiyatroyaGitmekIsterMisin;
    public AudioClip tiyatroyaCagirdiMi;


    public AudioClip snd_aferin;
    public AudioClip snd_tekrarDusun;
    public AudioClip snd_alkis;
    public AudioClip snd_merhaba;
    public AudioClip snd_yemekyemeyegidiyorum;
    public AudioClip snd_yemekeCagirdimi;
    public AudioClip snd_oyunoynamayagidiyorum;
    public AudioClip snd_oyunaCagirdimi;

    AudioSource _myAudioSource;
    public GameObject yesNoPanel;
    public GameObject talkBubbleA;
    public GameObject talkBubbleB;

    public GameObject talkBubbleC;
    public GameObject talkBubbleD;

    public GameObject[] victoryGos;
    public Image fader;
    CameraMovementController _myCamMov;

    BADU_GSConnect googleConnectCntrl;
    string PlUserName = " ";
    string moduleName = " ";
    string QuestionName = " ";
    int answerCount = 1;
    float correctAnswerTime = 0;

    void Start()
    {
        string nameGet = PlayerPrefs.GetString("CurrentPlayerName");
        PlUserName = nameGet;
        googleConnectCntrl = GameObject.FindGameObjectWithTag("GoogleConnect").GetComponent<BADU_GSConnect>();

        _myAudioSource = GetComponent<AudioSource>();
        yesNoPanel.transform.DOLocalMoveY(1000f, 1f, true);
        fader.raycastTarget = false;
        fader.DOFade(0f, 1f);
        _myCamMov = Camera.main.GetComponent<CameraMovementController>();   //

        //start stop to buffer camera victory
        for (int i = 0; i < victoryGos.Length; i++)
        {
            victoryGos[i].SetActive(true);
            victoryGos[i].SetActive(false);
        }

        StartCoroutine("indirektKonusmaPartA");
    }

    public IEnumerator indirektKonusmaPartA()
    {
        _myAudioSource.PlayOneShot(snd_merhaba);
        while (_myAudioSource.isPlaying)
            yield return null;

        _myAudioSource.PlayOneShot(bazenDirekSoylemeyiz);
        while (_myAudioSource.isPlaying)
            yield return null;

        _myCamMov.BtoA();
        //question 1
        part2Pressed = false;
        yield return new WaitForSeconds(3f);
        talkBubbleA.transform.DOScale(1f, 1f);

        _myAudioSource.PlayOneShot(merhabaCan);
        while (_myAudioSource.isPlaying)
            yield return null;

        _myAudioSource.PlayOneShot(snd_yemekyemeyegidiyorum);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1f);
        _myCamMov.AtoB();
        talkBubbleA.transform.DOScale(0f, 0.3f);

        yield return new WaitForSeconds(3f);

        talkBubbleC.transform.DOScale(1f, 1f);

        _myAudioSource.PlayOneShot(snd_yemekeCagirdimi);

        while (_myAudioSource.isPlaying)
            yield return null;

        correctAnswerTime = Time.time;

        yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);

        talkBubbleC.transform.DOScale(0f, 0.3f);

        currCorrectAnswer = true;
        while (!part2Pressed) { yield return null; }
        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul3", "Soru1", 0, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(3);

        _myAudioSource.PlayOneShot(benzerOrnek);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1);
        _myCamMov.BtoA();
        yield return new WaitForSeconds(3f);

        _myAudioSource.PlayOneShot(annemKekYapti);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1);
        _myCamMov.AtoB();
        yield return new WaitForSeconds(3f);

        _myAudioSource.PlayOneShot(kekYemeyeCagirdiMi);
        while (_myAudioSource.isPlaying)
            yield return null;

        yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);

        part2Pressed = false;
        currCorrectAnswer = true;
        while (!part2Pressed) { yield return null; }
        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul3", "Soru2", 0, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(3);

        _myAudioSource.PlayOneShot(baskaOrnek);
        while (_myAudioSource.isPlaying)
            yield return null;

        _myAudioSource.PlayOneShot(beraberBaskaOrnekBakalim);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(2f);
        _myCamMov.BtoA();
        yield return new WaitForSeconds(3f);

        //PART 2 
        part2Pressed = false;

        //yield return new WaitForSeconds(3f);
        talkBubbleB.transform.DOScale(1f, 1f);
        //yield return new WaitForSeconds(1f);

        _myAudioSource.PlayOneShot(merhabaSevgi);
        while (_myAudioSource.isPlaying)
            yield return null;

        _myAudioSource.PlayOneShot(snd_oyunoynamayagidiyorum);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1f);
        _myCamMov.AtoB();
        talkBubbleB.transform.DOScale(0f, 0.3f);
        yield return new WaitForSeconds(3f);
        talkBubbleD.transform.DOScale(1f, 1f);

        _myAudioSource.PlayOneShot(snd_oyunaCagirdimi);

        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1f);
        talkBubbleD.transform.DOScale(0f, 0.3f);
        correctAnswerTime = Time.time;

        yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);
        currCorrectAnswer = true;
        while (!part2Pressed) { yield return null; }
        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul3", "Soru3", 0, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(3);


        _myAudioSource.PlayOneShot(benzerOrnek);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1);
        _myCamMov.BtoA();
        yield return new WaitForSeconds(3f);

        _myAudioSource.PlayOneShot(merhabaSevgi);
        while (_myAudioSource.isPlaying)
            yield return null;

        _myAudioSource.PlayOneShot(tiyatroyaGitmekIsterMisin);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1);
        _myCamMov.AtoB();
        yield return new WaitForSeconds(3f);

        _myAudioSource.PlayOneShot(tiyatroyaCagirdiMi);
        while (_myAudioSource.isPlaying)
            yield return null;

        yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);
        part2Pressed = false;
        currCorrectAnswer = true;
        while (!part2Pressed) { yield return null; }
        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul3", "Soru4", 0, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(2);

        //start camera victory
        for (int i = 0; i < victoryGos.Length; i++)
        {
            if (i == 2)
            {//girl
                if (PlayerPrefs.GetInt("SelectedCharacter") == 0) victoryGos[i].SetActive(true);
            }
            else if (i == 3)
            {//boy
                if (PlayerPrefs.GetInt("SelectedCharacter") == 1) victoryGos[i].SetActive(true);
            }
            else
            {
                victoryGos[i].SetActive(true);
            }
        }
        _myAudioSource.PlayOneShot(snd_alkis);


        //record this module finished for player
        if (PlayerPrefs.HasKey(PlUserName))
        {
            int currentModulesFinished = PlayerPrefs.GetInt(PlUserName);
            string levelCompletedCode = currentModulesFinished.ToString();
            if (levelCompletedCode[3] != levelCompletedCode[0])
            {
                currentModulesFinished = currentModulesFinished + 00100;///first module finished. Module counts from left
				PlayerPrefs.SetInt(PlUserName, currentModulesFinished);
                PlayerPrefs.Save();
            }
        }


        yield return new WaitForSeconds(11f);
        fader.raycastTarget = true;
        fader.DOFade(1f, 1f);
        yield return new WaitForSeconds(1f);
        Application.LoadLevel("MainMenu");
        yield break;
    }

    bool part2Pressed = false;
    bool currCorrectAnswer = false;

    public void part2AnswerReceived(bool isYes)
    {
        if (isYes == currCorrectAnswer)
        {
            //answer is correct so move up
            yesNoPanel.transform.DOLocalMoveY(1000f, 1f, true);
            part2Pressed = true;
            _myAudioSource.PlayOneShot(snd_aferin);
        }
        else
        {
            _myAudioSource.PlayOneShot(snd_tekrarDusun);
        }
    }

}
