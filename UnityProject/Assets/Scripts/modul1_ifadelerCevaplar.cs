﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class modul1_ifadelerCevaplar : MonoBehaviour , IPointerClickHandler {
	public int _myCevapId = 0;
	public Modul1_ifadelerNew _myManager;
	public RawImage _myRImage;
	public bool isTrueAnswer = false;
	Button _myButton;
	void Awake () {
		_myManager = GetComponentInParent<Modul1_ifadelerNew> ();
		_myRImage = GetComponent<RawImage> ();
		_myButton = GetComponent<Button> ();
	}
	
	public void OnPointerClick(PointerEventData data){
		if(isTrueAnswer)_myButton.interactable = false;

		Debug.Log("clicked" + this.name);
		//_myManager.cevapId = _myCevapId;
		_myManager.DogruCevapGeldi = isTrueAnswer;
		_myManager.isCevapGeldi = true;
	}
}
