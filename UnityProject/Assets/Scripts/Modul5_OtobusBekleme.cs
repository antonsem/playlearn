﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;


public class Modul5_OtobusBekleme : MonoBehaviour
{
    public Turn teacherTurn;
    public AudioClip snd_aferin;
    public AudioClip snd_tekrarDusun;
    public AudioClip snd_alkis;
    public AudioClip snd_siraBeklemelisin;
    public AudioClip snd_beklemeliMisin;
    AudioSource _myAudioSource;
    public GameObject yesNoPanel;
    public GameObject talkBubble;
    public GameObject[] victoryGos;
    public Image fader;

    BADU_GSConnect googleConnectCntrl;
    string PlUserName = " ";
    string moduleName = " ";
    string QuestionName = " ";
    int answerCount = 1;
    float correctAnswerTime = 0;

    void Start()
    {
        string nameGet = PlayerPrefs.GetString("CurrentPlayerName");
        PlUserName = nameGet;
        googleConnectCntrl = GameObject.FindGameObjectWithTag("GoogleConnect").GetComponent<BADU_GSConnect>();

        _myAudioSource = GetComponent<AudioSource>();

        //start stop to buffer camera victory
        for (int i = 0; i < victoryGos.Length; i++)
        {
            victoryGos[i].SetActive(true);
            victoryGos[i].SetActive(false);
        }

        StartCoroutine("otobusBekleme");
        yesNoPanel.transform.DOLocalMoveY(1000f, 1f, true);
        fader.raycastTarget = false;
        fader.DOFade(0f, 1f);
    }

    public IEnumerator otobusBekleme()
    {
        //question 1
        part2Pressed = false;
        teacherTurn.TurnToPos(0);
        yield return new WaitForSeconds(3f);
        Camera.main.GetComponent<CameraMovementController>().BtoA();

        yield return new WaitForSeconds(3f);
        talkBubble.transform.DOScale(1f, 1f);
        yield return new WaitForSeconds(1f);

        _myAudioSource.PlayOneShot(snd_siraBeklemelisin);

        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1f);

        _myAudioSource.PlayOneShot(snd_beklemeliMisin);
        while (_myAudioSource.isPlaying)
            yield return null;

        teacherTurn.TurnToPos(1);
        Camera.main.GetComponent<CameraMovementController>().ToPos(0);
        correctAnswerTime = Time.time;

        yield return new WaitForSeconds(1f);
        yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);
        currCorrectAnswer = true;
        while (!part2Pressed) { yield return null; }
        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul5_Otobus", "Soru1", 0, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(2f);

        //start camera victory
        for (int i = 0; i < victoryGos.Length; i++)
            victoryGos[i].SetActive(true);

        _myAudioSource.PlayOneShot(snd_alkis);

        //record this module finished for player
        if (PlayerPrefs.HasKey(PlUserName))
        {
            int currentModulesFinished = PlayerPrefs.GetInt(PlUserName);
            string levelCompletedCode = currentModulesFinished.ToString();
            if (levelCompletedCode[4] != levelCompletedCode[0])
            {
                currentModulesFinished = currentModulesFinished + 00010;///first module finished. Module counts from left
				PlayerPrefs.SetInt(PlUserName, currentModulesFinished);
                PlayerPrefs.Save();
            }
        }

        yield return new WaitForSeconds(11f);
        fader.raycastTarget = true;
        fader.DOFade(1f, 1f);
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("MainMenu");
        yield break;
    }

    bool part2Pressed = false;
    bool currCorrectAnswer = false;

    public void part2AnswerReceived(bool isYes)
    {
        if (isYes == currCorrectAnswer)
        {
            //answer is correct so move up
            yesNoPanel.transform.DOLocalMoveY(1000f, 1f, true);
            part2Pressed = true;
            _myAudioSource.PlayOneShot(snd_aferin);
        }
        else
        {
            _myAudioSource.PlayOneShot(snd_tekrarDusun);
        }
    }

}
