﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;


public class modul2_yiyecekler : MonoBehaviour
{

    public AudioClip snd_merhaba;
    public AudioClip snd_aferin;
    public AudioClip snd_tekrarDusun;
    public AudioClip snd_alkis;
    public AudioClip snd_neyemekistersin;
    public AudioClip snd_neicmekistersin;

    public AudioClip snd_elma;
    public AudioClip snd_muz;
    public AudioClip snd_nar;
    public AudioClip snd_meyvasuyu;
    public AudioClip snd_ayran;
    public AudioClip snd_sut;

    public AudioClip snd_can_elma;
    public AudioClip snd_can_muz;
    public AudioClip snd_can_nar;
    public AudioClip snd_can_meyvasuyu;
    public AudioClip snd_can_ayran;
    public AudioClip snd_can_sut;

    bool isGirlSelected = false;

    AudioSource _myAudioSource;
    public GameObject talkBubbleYemek;
    public GameObject talkBubbleIcecek;

    public GameObject panelYemek;
    public GameObject PanelIcecek;

    public GameObject[] victoryGos;

    public RawImage teacherImg;
    public Image fader;

    BADU_GSConnect googleConnectCntrl;
    string PlUserName = " ";
    string moduleName = " ";
    string QuestionName = " ";
    int answerCount = 1;
    float correctAnswerTime = 0;
    int buttonPressCount = 0;

    void Start()
    {
        buttonPressCount = 0;
        if (PlayerPrefs.GetInt("SelectedCharacter") > 0)
        {
            //its a boy!
            isGirlSelected = false;
        }
        else
        {
            //girl
            isGirlSelected = true;
        }

        string nameGet = PlayerPrefs.GetString("CurrentPlayerName");
        PlUserName = nameGet;
        googleConnectCntrl = GameObject.FindGameObjectWithTag("GoogleConnect").GetComponent<BADU_GSConnect>();

        _myAudioSource = GetComponent<AudioSource>();
        fader.DOFade(0f, 1f);
        fader.raycastTarget = false;

        //start stop to buffer camera victory
        for (int i = 0; i < victoryGos.Length; i++)
        {
            victoryGos[i].SetActive(true);
            victoryGos[i].SetActive(false);
        }
        StartCoroutine("Yiyecekler");
    }

    public void ButtonPressed()
    {
        buttonPressCount++;
        Debug.Log("yeme icme button pressed = " + buttonPressCount);
    }

    IEnumerator Yiyecekler()
    {
        buttonPressCount = 0;
        yield return new WaitForSeconds(3f);
        //question 1
        teacherImg.DOFade(1f, 1f);
        _myAudioSource.PlayOneShot(snd_merhaba);

        questionAnswered = false;
        yield return new WaitForSeconds(3f);
        _myAudioSource.PlayOneShot(snd_neyemekistersin);
        talkBubbleYemek.transform.DOScale(1f, 1f);
        yield return new WaitForSeconds(1f);

        panelYemek.transform.DOLocalMoveY(0f, 1f, true);

        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(2f);
        correctAnswerTime = Time.time;
        teacherImg.DOFade(0f, 1f);
        talkBubbleYemek.transform.DOScale(0f, 1f);

        while (!questionAnswered) { yield return null; }

        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul2 Yiyecekler", selectedName, 1, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(2f);
        panelYemek.transform.DOLocalMoveY(1000f, 1f, true);


        StartCoroutine("Icecekler");
        yield break;
    }

    IEnumerator Icecekler()
    {
        buttonPressCount = 0;

        //question 2
        questionAnswered = false;
        yield return new WaitForSeconds(1f);
        teacherImg.DOFade(1f, 1f);

        talkBubbleIcecek.transform.DOScale(1f, 1f);
        yield return new WaitForSeconds(1f);

        _myAudioSource.PlayOneShot(snd_neicmekistersin);
        PanelIcecek.transform.DOLocalMoveY(0f, 1f, true);
        while (_myAudioSource.isPlaying)
        {
            yield return null;
        }
        yield return new WaitForSeconds(2f);
        correctAnswerTime = Time.time;

        teacherImg.DOFade(0f, 1f);
        talkBubbleIcecek.transform.DOScale(0f, 1f);

        while (!questionAnswered) { yield return null; }
        //yield return new WaitForSeconds(3f);

        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul2 icecekler", selectedName, 1, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(3f);
        PanelIcecek.transform.DOLocalMoveY(-1000f, 1f, true);


        yield return new WaitForSeconds(2f);

        //start camera victory
        for (int i = 0; i < victoryGos.Length; i++)
            victoryGos[i].SetActive(true);

        _myAudioSource.PlayOneShot(snd_alkis);

        //record this module finished for player
        if (PlayerPrefs.HasKey(PlUserName))
        {
            int currentModulesFinished = PlayerPrefs.GetInt(PlUserName);
            string levelCompletedCode = currentModulesFinished.ToString();
            Debug.Log(levelCompletedCode + "  " + levelCompletedCode[2]);
            if (levelCompletedCode[2] != levelCompletedCode[0])
            {
                currentModulesFinished = currentModulesFinished + 01000;///first module finished. Module counts from left
				PlayerPrefs.SetInt(PlUserName, currentModulesFinished);
                PlayerPrefs.Save();
            }
        }

        yield return new WaitForSeconds(11f);
        fader.DOFade(1f, 1f);
        fader.raycastTarget = true;

        yield return new WaitForSeconds(1f);
        Application.LoadLevel("MainMenu");
        yield break;

    }

    public bool questionAnswered = false;
    bool currCorrectAnswer = false;
    string selectedName = "";

    public CanvasGroup mainCanvasGroup;

    public void part2AnswerReceived(int wantedFood)
    {
        if (!questionAnswered)
            StartCoroutine(part2AnswerReceivedActual(wantedFood));
    }

    IEnumerator part2AnswerReceivedActual(int wantedFood)
    {
        mainCanvasGroup.interactable = false;
        mainCanvasGroup.blocksRaycasts = false;

        switch (wantedFood)
        {
            case 1:
                {
                    selectedName = "elma";
                    _myAudioSource.PlayOneShot(snd_elma);
                    break;
                }
            case 2:
                {
                    selectedName = "muz";
                    _myAudioSource.PlayOneShot(snd_muz);
                    break;
                }
            case 3:
                {
                    selectedName = "nar";
                    _myAudioSource.PlayOneShot(snd_nar);
                    break;
                }
            case 4:
                {
                    selectedName = "ayran";
                    _myAudioSource.PlayOneShot(snd_can_ayran);
                    break;
                }
            case 5:
                {
                    selectedName = "sut";
                    _myAudioSource.PlayOneShot(snd_can_sut);
                    break;
                }
            case 6:
                {
                    selectedName = "meyvasuyu";
                    _myAudioSource.PlayOneShot(snd_can_meyvasuyu);
                    break;
                }
        }

        while (_myAudioSource.isPlaying)
            yield return null;

        mainCanvasGroup.interactable = true;
        mainCanvasGroup.blocksRaycasts = true;

        questionAnswered = true;

        yield break;
    }
}
