﻿using UnityEngine;
using System.Collections;

public class PlayVid : MonoBehaviour
{
    IEnumerator Start()
    {
        Handheld.PlayFullScreenMovie("angryMan.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        yield return new WaitForSeconds(1);
        Application.LoadLevel("MainMenu");
    }
}
