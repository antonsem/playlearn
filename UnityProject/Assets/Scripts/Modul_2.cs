﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class Modul_2 : MonoBehaviour
{
    public Animator foodCanvasAnim;
    public Image foodImage;
    public AudioSource source;
    public CameraMovementController cam;
    public Image mainPanel;
    public Animator mainPanelAnimator;
    public Turn teacherTurn;
    public GameObject can;
    public GameObject sevgi;
    public GameObject winPanel;

    #region AudioClips
    public AudioClip ogrMerhaba;
    public AudioClip ogrNeYemekIstersin;
    public AudioClip ogrSenNeYemekIstersin;
    public AudioClip ogrNeIcmekIstersin;
    public AudioClip ogrSenNeIcmekIstersin;

    public AudioClip canMuzYemekIsterim;
    public AudioClip canSutIcmekIsterim;

    public AudioClip sevgiElmaYemekIsterim;
    public AudioClip sevgiAyranIcmekIsterim;

    public AudioClip applause;
    public AudioClip music;

    public AudioClip[] foodAnswers;
    #endregion

    public Sprite[] foodSprites;
    private bool answered = false;
    BADU_GSConnect googleConnectCntrl;
    float correctAnswerTime = 0;
    string PlUserName = " ";
    string selectedName = "";
    private Dictionary<int, string> foodDict = new Dictionary<int, string>()
    { {0, "muz" }, { 1, "elma"}, {2, "nar" }, {3, "sut" }, {4, "meyvasuyu" }, {5, "ayran" } };

    void Start()
    {
        googleConnectCntrl = GameObject.FindGameObjectWithTag("GoogleConnect").GetComponent<BADU_GSConnect>();
        PlUserName = PlayerPrefs.GetString("CurrentPlayerName"); ;

        teacherTurn.TurnToPos(0);
        StartCoroutine(NeIstersin());
    }

    private IEnumerator NeIstersin()
    {
        yield return StartCoroutine(SetPanelAlpha(0));
        yield return StartCoroutine(PlayAudio(ogrMerhaba));
        yield return StartCoroutine(PlayAudio(ogrNeYemekIstersin));

        foodCanvasAnim.SetTrigger("ShowFood");
        yield return StartCoroutine(PlayAudio(canMuzYemekIsterim));
        foodCanvasAnim.SetTrigger("HideFood");

        yield return StartCoroutine(SetPanelAlpha(1));
        can.SetActive(false);
        sevgi.SetActive(true);
        yield return new WaitForSeconds(1);
        yield return StartCoroutine(SetPanelAlpha(0));
        yield return StartCoroutine(PlayAudio(ogrMerhaba));
        yield return StartCoroutine(PlayAudio(ogrNeYemekIstersin));
        foodImage.sprite = foodSprites[1];
        foodCanvasAnim.SetTrigger("ShowFood");
        yield return StartCoroutine(PlayAudio(sevgiElmaYemekIsterim));
        foodCanvasAnim.SetTrigger("HideFood");

        cam.ToPos(1);
        teacherTurn.TurnToPos(1);
        yield return new WaitForSeconds(2);
        yield return StartCoroutine(PlayAudio(ogrSenNeYemekIstersin));


        yield return StartCoroutine(SetPanelAlpha(1));
        mainPanelAnimator.SetTrigger("FoodQuestion");
        correctAnswerTime = Time.time;
        answered = false;
        while (!answered)
            yield return null;

        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul2 Yiyecekler", selectedName, 1, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(3);

        can.SetActive(true);
        sevgi.SetActive(false);

        yield return StartCoroutine(SetPanelAlpha(0));
        mainPanelAnimator.SetTrigger("HideFood");
        cam.ToPos(0);
        teacherTurn.TurnToPos(0);

        yield return new WaitForSeconds(1);
        yield return StartCoroutine(PlayAudio(ogrMerhaba));
        yield return StartCoroutine(PlayAudio(ogrNeIcmekIstersin));

        foodImage.sprite = foodSprites[2];
        foodCanvasAnim.SetTrigger("ShowFood");
        yield return StartCoroutine(PlayAudio(canSutIcmekIsterim));
        foodCanvasAnim.SetTrigger("HideFood");

        yield return StartCoroutine(SetPanelAlpha(1));
        can.SetActive(false);
        sevgi.SetActive(true);
        yield return new WaitForSeconds(1);
        yield return StartCoroutine(SetPanelAlpha(0));
        yield return StartCoroutine(PlayAudio(ogrMerhaba));
        yield return StartCoroutine(PlayAudio(ogrNeIcmekIstersin));
        foodImage.sprite = foodSprites[3];
        foodCanvasAnim.SetTrigger("ShowFood");
        yield return StartCoroutine(PlayAudio(sevgiAyranIcmekIsterim));
        foodCanvasAnim.SetTrigger("HideFood");

        cam.ToPos(1);
        teacherTurn.TurnToPos(1);
        yield return new WaitForSeconds(1);
        yield return StartCoroutine(PlayAudio(ogrSenNeIcmekIstersin));
        StartCoroutine(SetPanelAlpha(1));
        mainPanelAnimator.SetTrigger("DrinkQuestion");
        correctAnswerTime = Time.time;
        answered = false;
        while (!answered)
            yield return null;

        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul2 icecekler", selectedName, 1, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(3);

        //yield return StartCoroutine(SetPanelAlpha(0));
        mainPanelAnimator.SetTrigger("HideFood");

        Win();

        yield return new WaitForSeconds(10);
        SceneManager.LoadScene("MainMenu");
    }

    private IEnumerator PlayAudio(AudioClip clip)
    {
        source.PlayOneShot(clip);
        while (source.isPlaying)
            yield return null;
    }

    private IEnumerator SetPanelAlpha(float newAlpha)
    {
        Color newColor = mainPanel.color;
        newColor.a = newAlpha;
        while (Mathf.Abs(mainPanel.color.a - newAlpha) > 0.05f)
        {
            mainPanel.color = Color.Lerp(mainPanel.color, newColor, 0.1f);
            yield return null;
        }
        mainPanel.color = newColor;
    }

    public void FoodAnwer(int index)
    {
        if (answered)
            return;
        selectedName = foodDict[index];
        StartCoroutine(PlayAudio(foodAnswers[index]));
        answered = true;
    }

    private void Win()
    {
        StartCoroutine(PlayAudio(applause));
        StartCoroutine(PlayAudio(music));
        winPanel.SetActive(true);
        if (PlayerPrefs.HasKey(PlUserName))
        {
            int currentModulesFinished = PlayerPrefs.GetInt(PlUserName);
            string levelCompletedCode = currentModulesFinished.ToString();
            Debug.Log(levelCompletedCode + "  " + levelCompletedCode[2]);
            if (levelCompletedCode[2] != levelCompletedCode[0])
            {
                currentModulesFinished = currentModulesFinished + 01000;///first module finished. Module counts from left
				PlayerPrefs.SetInt(PlUserName, currentModulesFinished);
                PlayerPrefs.Save();
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            source.Stop();
    }
}
