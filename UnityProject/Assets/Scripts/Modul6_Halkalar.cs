﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using RootMotion.FinalIK;

public class Modul6_Halkalar : MonoBehaviour
{
    public AudioClip snd_aferin;
    public AudioClip snd_tekrarDusun;
    public AudioClip snd_victory;
    public AudioClip snd_alkis;
    public AudioClip snd_merhaba;

    public AudioClip ogr_tebrikEtmeliyiz;
    public AudioClip ogr_tesekkurEtmeliyiz;
    public AudioClip ogr_ornek;
    public AudioClip ogr_simdihalkalari;
    public AudioClip ogr_sevgisimdiBeklemelisin;

    public AudioClip snd_Sevgi_tebrikederim;
    public AudioClip snd_Can_tesekkurederim;

    public AudioClip ogr_sevgidogrucumleyikurdumu;
    public AudioClip ogr_sevgigozlerinebakmalımıydı;

    public AudioClip ogr_sevgihalkalariatcantebriket;
    public AudioClip ogr_sevgiTesekkurEtmelisin;

    public AudioClip snd_Can_tebrikederim;
    public AudioClip snd_Sevgi_tesekkurederim;

    public AudioClip ogr_candogrucumleyikurdumu;
    public AudioClip ogr_cangozlerinebakmalımıydı;

    public AudioClip snd_halkalarCubugaA;
    public AudioClip snd_halkalarCubugaB;


    public GameObject OgretmenAnlatmaGo;
    public GameObject SevgiHalkaAtmaGo;
    public GameObject CanHalkaAtmaGo;

    public GameObject SevgiHalkaGo;
    public GameObject CanHalkaGo;
    public GameObject gidenHalkaGo;
    public GameObject sevgi_gidenHalkaGo;

    public Image insceneFader;

    public Animator CanAnimator;
    public Animator SevgiAnimator;
    public Animator GidenHalkaAnimator;

    private Turn canTurn;
    private Turn sevgiTurn;

    AudioSource _myAudioSource;
    public GameObject yesNoPanel;

    public GameObject TB_OgrSimdihalkalari;
    public GameObject TB_OgrSevgiBekle;
    public GameObject TB_SevgiTebrik;
    public GameObject TB_CanTesekkur;
    public GameObject TB_OgrCanBekle;
    public GameObject TB_CanTebrik;
    public GameObject TB_SevgiTesekkur;

    public GameObject[] victoryGos;
    public Image fader;
    CameraMovementController _myCamMov;

    BADU_GSConnect googleConnectCntrl;
    string PlUserName = " ";
    string moduleName = " ";
    string QuestionName = " ";
    int answerCount = 1;
    float correctAnswerTime = 0;
    public BipedIK teacherBiped;
    public Transform[] targetId; //0 : boy, 1 : cam , 2 : girl
    public Transform targetActual; //0 : boy, 1 : cam , 2 : girl

    void Start()
    {
        canTurn = CanAnimator.gameObject.GetComponent<Turn>();
        sevgiTurn = SevgiAnimator.gameObject.GetComponent<Turn>();

        gidenHalkaGo.SetActive(false);
        string nameGet = PlayerPrefs.GetString("CurrentPlayerName");
        PlUserName = nameGet;
        googleConnectCntrl = GameObject.FindGameObjectWithTag("GoogleConnect").GetComponent<BADU_GSConnect>();

        _myAudioSource = GetComponent<AudioSource>();
        yesNoPanel.transform.DOLocalMoveY(1000f, 1f, true);
        fader.raycastTarget = false;
        fader.DOFade(0f, 1f);
        _myCamMov = Camera.main.GetComponent<CameraMovementController>();

        //start stop to buffer camera victory
        for (int i = 0; i < victoryGos.Length; i++)
        {
            victoryGos[i].SetActive(true);
            victoryGos[i].SetActive(false);
        }

        StartCoroutine("Halkalar");
        targetActual.DOMove(targetId[1].position, 0f, false);
    }

    public IEnumerator Halkalar()
    {
        //part 1
#if true
        _myCamMov.ToPos(0);
        canTurn.TurnToPos(2);
        sevgiTurn.TurnToPos(2);
        part2Pressed = false;

        _myAudioSource.PlayOneShot(snd_merhaba);

        while (_myAudioSource.isPlaying)
            yield return null;

        _myAudioSource.PlayOneShot(ogr_tebrikEtmeliyiz);

        while (_myAudioSource.isPlaying)
            yield return null;

        _myAudioSource.PlayOneShot(ogr_ornek);

        while (_myAudioSource.isPlaying)
            yield return null;

        _myCamMov.ToPos(1);

        yield return new WaitForSeconds(1f);
        TB_OgrSimdihalkalari.transform.DOScale(1f, 1f);
        yield return new WaitForSeconds(1f);

        _myAudioSource.PlayOneShot(ogr_simdihalkalari);

        yield return new WaitForSeconds(2f);
        targetActual.DOMove(targetId[0].position, 0.7f, false);

        while (_myAudioSource.isPlaying)
            yield return null;

        TB_OgrSimdihalkalari.transform.DOScale(0f, 0.3f);
        yield return new WaitForSeconds(1f);

        targetActual.DOMove(targetId[2].position, 0.7f, false);

        yield return new WaitForSeconds(1f);
        TB_OgrSevgiBekle.transform.DOScale(1f, 1f);

        _myAudioSource.PlayOneShot(ogr_sevgisimdiBeklemelisin);

        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1f);
        TB_OgrSevgiBekle.transform.DOScale(0f, 0.3f);
        yield return new WaitForSeconds(1f);
        targetActual.DOMove(targetId[1].position, 0.7f, false);

        //fade out
        //insceneFader.DOFade(1f, 1f);
        _myCamMov.ToPos(2);
        canTurn.TurnToPos(0);
        sevgiTurn.TurnToPos(0);
        yield return new WaitForSeconds(1f);
        //OgretmenAnlatmaGo.SetActive(false);
        CanHalkaAtmaGo.SetActive(true);
        yield return new WaitForSeconds(1f);
        //insceneFader.DOFade(0f, 0.5f);
        //fade in. 

        //teacherBiped.SetLookAtPosition (targetId [1].position);
        //teacherBiped.SetLookAtWeight (1, 0.5f, 1f, 1f, 0.5f, 0.7f, 0.5f);//full

        //evet simdi halkalar cubuga
        _myAudioSource.PlayOneShot(snd_halkalarCubugaA);
        //		while (_myAudioSource.isPlaying) {
        //			yield return null;
        //		}

        //Can halka atma
        yield return new WaitForSeconds(3f);
        CanAnimator.SetTrigger("atis");

        //wait 17 frames

        yield return new WaitForSeconds(1.5f);
        _myAudioSource.PlayOneShot(snd_alkis);

        while (_myAudioSource.isPlaying)
            yield return null;

        //fade out
        //insceneFader.DOFade(1f, 1f);
        _myCamMov.ToPos(1);
        canTurn.TurnToPos(1);
        sevgiTurn.TurnToPos(1);
        yield return new WaitForSeconds(1f);
        gidenHalkaGo.SetActive(false);
        //CanHalkaAtmaGo.SetActive(false);
        //OgretmenAnlatmaGo.SetActive(true);
        yield return new WaitForSeconds(1f);
        //insceneFader.DOFade(0f, 0.5f);
        //fade in. 

        //teacherBiped.SetLookAtPosition (targetId [2].position);
        //teacherBiped.SetLookAtWeight (1, 0.5f, 1f, 1f, 0.5f, 0.7f, 0.5f);//full
        targetActual.DOMove(targetId[2].position, 0.7f, false);

        //Sevgi tebrik etme
        yield return new WaitForSeconds(1f);
        TB_SevgiTebrik.transform.DOScale(1f, 1f);
        yield return new WaitForSeconds(1f);
        _myAudioSource.PlayOneShot(snd_Sevgi_tebrikederim);

        while (_myAudioSource.isPlaying)
            yield return null;
        
        yield return new WaitForSeconds(1f);
        TB_SevgiTebrik.transform.DOScale(0f, 0.3f);

        //teacherBiped.SetLookAtPosition (targetId [0].position);
        //teacherBiped.SetLookAtWeight (1, 0.5f, 1f, 1f, 0.5f, 0.7f, 0.5f);//full
        targetActual.DOMove(targetId[0].position, 0.7f, false);

        //Can Tesekkur etme
        TB_CanTesekkur.transform.DOScale(1f, 1f);
        yield return new WaitForSeconds(1f);
        _myAudioSource.PlayOneShot(snd_Can_tesekkurederim);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1f);
        TB_CanTesekkur.transform.DOScale(0f, 0.3f);
        yield return new WaitForSeconds(1f);

        //teacherBiped.SetLookAtPosition (targetId [1].position);
        //teacherBiped.SetLookAtWeight (1, 0.5f, 1f, 1f, 0.5f, 0.7f, 0.5f);//full
        targetActual.DOMove(targetId[1].position, 0.7f, false);

        _myCamMov.ToPos(0);
        yield return new WaitForSeconds(1f);

        //soru 1
        part2Pressed = false;
        _myAudioSource.PlayOneShot(ogr_sevgidogrucumleyikurdumu);

        while (_myAudioSource.isPlaying)
            yield return null;

        correctAnswerTime = Time.time;

        yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);

        currCorrectAnswer = true;
        while (!part2Pressed) { yield return null; }
        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul6", "Soru1", 0, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(2f);

        //soru 2
        part2Pressed = false;
        _myAudioSource.PlayOneShot(ogr_sevgigozlerinebakmalımıydı);

        while (_myAudioSource.isPlaying)
            yield return null;

        correctAnswerTime = Time.time;

        yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);

        currCorrectAnswer = true;
        while (!part2Pressed) { yield return null; }
        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul6", "Soru2", 0, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(2f);

        //start part 2 sevgi halkalari atiyor
        StartCoroutine("HalkalarPart2");
        yield break;
    }

    public IEnumerator HalkalarPart2()
    {
        //part 1
        part2Pressed = false;
        //teacherBiped.SetLookAtPosition (targetId [2].position);
        //teacherBiped.SetLookAtWeight (1, 0.5f, 1f, 1f, 0.5f, 0.7f, 0.5f);//full
        yield return new WaitForSeconds(2);
        canTurn.TurnToPos(2);
        sevgiTurn.TurnToPos(2);
        _myAudioSource.PlayOneShot(ogr_tesekkurEtmeliyiz);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1);

        _myAudioSource.PlayOneShot(ogr_ornek);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1);
        _myCamMov.ToPos(1);
        yield return new WaitForSeconds(1f);

        targetActual.DOMove(targetId[2].position, 0.7f, false);

        yield return new WaitForSeconds(1f);
        TB_OgrCanBekle.transform.DOScale(1f, 1f);
        yield return new WaitForSeconds(1f);

        _myAudioSource.PlayOneShot(ogr_sevgihalkalariatcantebriket);
        yield return new WaitForSeconds(3.5f);
        targetActual.DOMove(targetId[0].position, 0.7f, false);

        while (_myAudioSource.isPlaying)
            yield return null;

        targetActual.DOMove(targetId[2].position, 0.7f, false);
        yield return new WaitForSeconds(1);
        _myAudioSource.PlayOneShot(ogr_sevgiTesekkurEtmelisin);
        while (_myAudioSource.isPlaying)
            yield return null;

        //teacherBiped.SetLookAtPosition (targetId [0].position);
        //teacherBiped.SetLookAtWeight (1, 0.5f, 1f, 1f, 0.5f, 0.7f, 0.5f);//full

        yield return new WaitForSeconds(1f);
        TB_OgrCanBekle.transform.DOScale(0f, 0.3f);
        yield return new WaitForSeconds(1f);
        targetActual.DOMove(targetId[1].position, 0.7f, false);

        //fade out
        //insceneFader.DOFade(1f, 1f);
        _myCamMov.ToPos(2);
        canTurn.TurnToPos(0);
        sevgiTurn.TurnToPos(0);
        yield return new WaitForSeconds(1f);
        //OgretmenAnlatmaGo.SetActive(false);
        //SevgiHalkaAtmaGo.SetActive(true);
        //SevgiHalkaGo.SetActive(true);
        //yield return new WaitForSeconds(1f);
        //insceneFader.DOFade(0f, 0.5f);
        //fade in. 

        //evet simdi halkalar cubuga
        _myAudioSource.PlayOneShot(snd_halkalarCubugaB);

        //		while (_myAudioSource.isPlaying) {
        //			yield return null;
        //		}


        //Can halka atma
        yield return new WaitForSeconds(3f);
        SevgiAnimator.SetTrigger("atis");

        yield return new WaitForSeconds(1.5f);

        _myAudioSource.PlayOneShot(snd_alkis);
        while (_myAudioSource.isPlaying)
            yield return null;

        //fade out
        //insceneFader.DOFade(1f, 1f);
        _myCamMov.ToPos(1);
        canTurn.TurnToPos(1);
        sevgiTurn.TurnToPos(1);
        yield return new WaitForSeconds(1f);
        gidenHalkaGo.SetActive(false);
        //SevgiHalkaAtmaGo.SetActive(false);
        //OgretmenAnlatmaGo.SetActive(true);
        yield return new WaitForSeconds(1f);
        //insceneFader.DOFade(0f, 0.5f);
        //fade in. 

        //teacherBiped.SetLookAtPosition (targetId [2].position);
        //teacherBiped.SetLookAtWeight (1, 0.5f, 1f, 1f, 0.5f, 0.7f, 0.5f);//full
        targetActual.DOMove(targetId[0].position, 0.7f, false);

        //Sevgi tebrik etme
        yield return new WaitForSeconds(1f);
        TB_CanTebrik.transform.DOScale(1f, 1f);
        yield return new WaitForSeconds(1f);

        _myAudioSource.PlayOneShot(snd_Can_tebrikederim);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1f);
        TB_CanTebrik.transform.DOScale(0f, 0.3f);
        yield return new WaitForSeconds(1f);

        //teacherBiped.SetLookAtPosition (targetId [0].position);
        //teacherBiped.SetLookAtWeight (1, 0.5f, 1f, 1f, 0.5f, 0.7f, 0.5f);//full
        targetActual.DOMove(targetId[2].position, 0.7f, false);

        //Can Tesekkur etme
        yield return new WaitForSeconds(1f);
        TB_SevgiTesekkur.transform.DOScale(1f, 1f);
        yield return new WaitForSeconds(1f);

        _myAudioSource.PlayOneShot(snd_Sevgi_tesekkurederim);
        while (_myAudioSource.isPlaying)
            yield return null;

        yield return new WaitForSeconds(1f);
        TB_SevgiTesekkur.transform.DOScale(0f, 0.3f);
        yield return new WaitForSeconds(1f);

        //teacherBiped.SetLookAtPosition (targetId [1].position);
        //teacherBiped.SetLookAtWeight (1, 0.5f, 1f, 1f, 0.5f, 0.7f, 0.5f);//full
        targetActual.DOMove(targetId[1].position, 0.7f, false);

        //soru 3
        part2Pressed = false;

        _myCamMov.ToPos(0);
        yield return new WaitForSeconds(1f);

        _myAudioSource.PlayOneShot(ogr_sevgidogrucumleyikurdumu);

        while (_myAudioSource.isPlaying)
            yield return null;
        correctAnswerTime = Time.time;

        yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);

        currCorrectAnswer = true;
        while (!part2Pressed) { yield return null; }
        googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul6", "Soru3", 0, Time.time - correctAnswerTime);

        yield return new WaitForSeconds(2f);

        ////soru 4
        //part2Pressed = false;
        //_myAudioSource.PlayOneShot(ogr_cangozlerinebakmalımıydı);
        //while (_myAudioSource.isPlaying)
        //{
        //    yield return null;
        //}
        //correctAnswerTime = Time.time;

        //yesNoPanel.transform.DOLocalMoveY(0f, 1f, true);

        //currCorrectAnswer = true;
        //while (!part2Pressed) { yield return null; }
        //googleConnectCntrl.SaveDataOnTheCloud(PlUserName, "Modul6", "Soru4", 0, Time.time - correctAnswerTime);

        //yield return new WaitForSeconds(2f);
#endif

        //start camera victory
        for (int i = 0; i < victoryGos.Length; i++)
            victoryGos[i].SetActive(true);

        _myAudioSource.PlayOneShot(snd_victory);


        //record this module finished for player
        if (PlayerPrefs.HasKey(PlUserName))
        {
            int currentModulesFinished = PlayerPrefs.GetInt(PlUserName);
            string levelCompletedCode = currentModulesFinished.ToString();
            if (levelCompletedCode[5] != levelCompletedCode[0])
            {
                currentModulesFinished = currentModulesFinished + 00001;///first module finished. Module counts from left
				PlayerPrefs.SetInt(PlUserName, currentModulesFinished);
                PlayerPrefs.Save();
            }
        }

        yield return new WaitForSeconds(11f);
        fader.raycastTarget = true;
        fader.DOFade(1f, 1f);
        yield return new WaitForSeconds(1f);
        Application.LoadLevel("MainMenu");
        yield break;
    }

    public void HalkaUcus(bool can)
    {
        //yield return new WaitForSeconds (Time.deltaTime * 17f);
        if (CanHalkaGo.activeSelf && can) CanHalkaGo.SetActive(false);
        if (SevgiHalkaGo.activeSelf && !can) SevgiHalkaGo.SetActive(false);
        gidenHalkaGo.transform.parent = null;
        gidenHalkaGo.SetActive(true);
        if (can)
            GidenHalkaAnimator.SetTrigger("ucusCan");
        else
            GidenHalkaAnimator.SetTrigger("ucusSevgi");
    }

    bool part2Pressed = false;
    bool currCorrectAnswer = false;

    public void part2AnswerReceived(bool isYes)
    {
        if (isYes == currCorrectAnswer)
        {
            //answer is correct so move up
            yesNoPanel.transform.DOLocalMoveY(1000f, 1f, true);
            part2Pressed = true;
            _myAudioSource.PlayOneShot(snd_aferin);
        }
        else
        {
            _myAudioSource.PlayOneShot(snd_tekrarDusun);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            _myAudioSource.Stop();
    }
}
