﻿using UnityEngine;
using System.Collections;

public class Turn : MonoBehaviour
{

    public Transform[] positions;

    public void TurnToPos(int index)
    {
        if(positions == null || positions.Length <= index)
        {
            Debug.LogError("Position index is out of range!");
            return;
        }

        StartCoroutine(TurnCoroutine(positions[index]));
    }

    IEnumerator TurnCoroutine(Transform t)
    {
        while (t.rotation != transform.rotation)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, t.rotation, 0.1f);
            yield return null;
        }
    }

}
