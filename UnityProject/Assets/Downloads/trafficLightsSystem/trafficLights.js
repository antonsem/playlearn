﻿#pragma strict

//script created by TERCO (2015)
//This script manages two traffic lights. Using a texture with the three colors (RYG), and the same texture on dark we can switch
//them and turn on and turn off the lights. When the redLight gameObject of trafficLights1 has the lighted Texture, the greenLight
//of the trafficLights2 will have the darked Texture. This is an example of how it works.
//You only have to change the ChangeInterval variable by the inspector to set up how long you want the periods between lights last


var traffic1 : GameObject; //stores trafficLights 1 prefab
var traffic2 : GameObject; //stores trafficLights 2 prefab
var traffic1RED : GameObject;
var traffic1REDs : GameObject;
var traffic1YELLOW : GameObject;
var traffic1GREEN : GameObject;
var traffic1GREENs : GameObject;
var traffic2RED : GameObject;
var traffic2REDs : GameObject;
var traffic2YELLOW : GameObject;
var traffic2GREEN : GameObject;
var traffic2GREENs : GameObject;
var traffic1DUDE : GameObject;
var traffic2DUDE : GameObject;

var redDudeTex : Texture;
var greenDudeTex : Texture;
var darkTexture : Texture;
var lightsTexture : Texture;

var changeInterval : float;
private var changeInterval2 : float;

function Start () {

//stores the initial variable set up by the inspector
changeInterval2 = changeInterval;

}

function Update () {

//countdown stored in changeInterval frame by frame
changeInterval-= Time.deltaTime;

//phase1. TrafficLights1 doesnt allow pass
//changeInterval2*0.65 means that the first period happens between the start and the 35% of time passed (1-0.65)
if(changeInterval > (changeInterval2*0.65) && changeInterval<=changeInterval2)
phase1();

 //phase1 of yellow lights
 //(0.65 - 0.15 = 0.5) means the yellow lights are turned on during 15% of time 
if(changeInterval>(changeInterval2*0.5) && changeInterval<=(changeInterval2*0.65))
phase11();	

////phase2. TrafficLights2 allows pass
if(changeInterval>(changeInterval2*0.15) && changeInterval<=(changeInterval2*0.5))
phase2(); 

//phase2 of yellow lights
if(changeInterval>(changeInterval2*0) && changeInterval<=(changeInterval2*0.15))
phase22();

 //when the timer reaches 0, the countdown starts again. (line 47 starts again we could say)
 if(changeInterval<=0)
 	changeInterval=changeInterval2;

} //end update function

function phase1 (){

traffic1RED.GetComponent.<Renderer>().material.mainTexture = lightsTexture;
  traffic2GREEN.GetComponent.<Renderer>().material.mainTexture = lightsTexture;
  
  traffic1GREEN.GetComponent.<Renderer>().material.mainTexture = darkTexture;  
  traffic2RED.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  
  // small lights
  traffic1REDs.GetComponent.<Renderer>().material.mainTexture = lightsTexture;
  traffic2GREENs.GetComponent.<Renderer>().material.mainTexture = lightsTexture;
  
  traffic1GREENs.GetComponent.<Renderer>().material.mainTexture = darkTexture;  
  traffic2REDs.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  // small lights
 
  traffic1YELLOW.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  traffic2YELLOW.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  
  traffic1DUDE.GetComponent.<Renderer>().material.mainTexture = greenDudeTex;
  traffic2DUDE.GetComponent.<Renderer>().material.mainTexture = redDudeTex;


}

function phase11 () {

traffic1RED.GetComponent.<Renderer>().material.mainTexture = lightsTexture; 
 	traffic2YELLOW.GetComponent.<Renderer>().material.mainTexture = lightsTexture;
 	
 	traffic2GREEN.GetComponent.<Renderer>().material.mainTexture = darkTexture;  
  	traffic1GREEN.GetComponent.<Renderer>().material.mainTexture = darkTexture;  
    
    traffic1REDs.GetComponent.<Renderer>().material.mainTexture = darkTexture;
    traffic2GREENs.GetComponent.<Renderer>().material.mainTexture = darkTexture;

}

function phase2 (){

traffic1RED.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  traffic2GREEN.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  
  traffic1GREEN.GetComponent.<Renderer>().material.mainTexture = lightsTexture;  
  traffic2RED.GetComponent.<Renderer>().material.mainTexture = lightsTexture;
  
  // small lights
  traffic1REDs.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  traffic2GREENs.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  
  traffic1GREENs.GetComponent.<Renderer>().material.mainTexture = lightsTexture;    
  traffic2REDs.GetComponent.<Renderer>().material.mainTexture = lightsTexture;
  // small lights
  
  traffic1YELLOW.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  traffic2YELLOW.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  
  traffic1DUDE.GetComponent.<Renderer>().material.mainTexture = redDudeTex;
  traffic2DUDE.GetComponent.<Renderer>().material.mainTexture = greenDudeTex;
  

}

function phase22 (){

traffic2RED.GetComponent.<Renderer>().material.mainTexture = lightsTexture;   
  traffic1YELLOW.GetComponent.<Renderer>().material.mainTexture = lightsTexture;
  
  traffic1GREEN.GetComponent.<Renderer>().material.mainTexture = darkTexture;  
  traffic2GREEN.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  
  traffic1GREENs.GetComponent.<Renderer>().material.mainTexture = darkTexture;
  traffic2REDs.GetComponent.<Renderer>().material.mainTexture = darkTexture;

}