using System.Collections;
using UnityEngine;
using LitJson;


public class BADU_GSConnect : MonoBehaviour
{
    string webServiceUrl = "https://script.google.com/macros/s/AKfycbwhI3RPSjJrC1Ejw4VUfCjzwBtPc-VRAyZjK_yJFNF9UqHxcScc/exec";
    string spreadsheetId = "1zFpOVG03VbCZt06TkwOqbV6Cm3YbvYHd9rXs095VwVE";
    string worksheetName = "GirisData";
    string password = "otizm2016oyun";
    public float maxWaitTime = 10f;
    string statisticsWorksheetName = "ToplananData";
    public bool debugMode;

    bool updating;
    string currentStatus;
    JsonData[] ssObjects;

    Rect guiBoxRect;
    Rect guiButtonRect;
    Rect guiButtonRect2;
    Rect guiButtonRect3;
    bool connected = false;




    IEnumerator Start()
    {
        DontDestroyOnLoad(this);
        updating = false;
        currentStatus = "Offline";

        guiBoxRect = new Rect(10, 10, 310, 140);
        guiButtonRect = new Rect(30, 40, 270, 30);
        guiButtonRect2 = new Rect(30, 75, 270, 30);
        guiButtonRect3 = new Rect(30, 110, 270, 30);
        Connect();
        yield break;

    }

    //	void OnGUI()
    //	{
    //		GUI.Box(guiBoxRect, currentStatus);
    //
    ////		if (GUI.Button(guiButtonRect, "Update From Google Spreadsheet"))
    ////		{
    ////			Connect();
    ////		}
    ////
    ////		saveToGS = GUI.Toggle(guiButtonRect2, saveToGS, "Save Stats To Google Spreadsheet");
    ////
    ////		if (GUI.Button(guiButtonRect3, "Reset Balls values"))
    ////		{
    ////			dataDestinationObject.SendMessage("ResetBalls");
    ////		}
    //	}

    void Connect()
    {
        if (updating)
            return;

        updating = true;
        StartCoroutine(GetData());

    }

    IEnumerator GetData()
    {
        string connectionString = webServiceUrl + "?ssid=" + spreadsheetId + "&sheet=" + worksheetName + "&pass=" + password + "&action=GetData";
        if (debugMode)
            Debug.Log("Connecting to webservice on " + connectionString);

        WWW www = new WWW(connectionString);

        float elapsedTime = 0.0f;
        currentStatus = "Establishing Connection... ";

        while (!www.isDone)
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime >= maxWaitTime)
            {
                currentStatus = "Max wait time reached, connection aborted.";
                Debug.Log(currentStatus);
                updating = false;
                break;
            }

            yield return null;
        }

        if (!www.isDone || !string.IsNullOrEmpty(www.error))
        {
            currentStatus = "Connection error after" + elapsedTime.ToString() + "seconds: " + www.error;
            Debug.LogError(currentStatus);
            updating = false;
            connected = false;
            yield break;
        }

        string response = www.text;
        Debug.Log(elapsedTime + " : " + response);
        currentStatus = "Connection stablished, parsing data...";

        if (response == "\"Incorrect Password.\"")
        {
            currentStatus = "Connection error: Incorrect Password.";
            Debug.LogError(currentStatus);
            updating = false;
            connected = false;
            yield break;
        }

        try
        {
            ssObjects = JsonMapper.ToObject<JsonData[]>(response);
        }
        catch
        {
            currentStatus = "Data error: could not parse retrieved data as json.";
            Debug.LogError(currentStatus);
            updating = false;
            connected = false;
            yield break;
        }

        currentStatus = "Data Successfully Retrieved!";
        updating = false;
        connected = true;

        // Finally use the retrieved data as you wish.
        //dataDestinationObject.SendMessage("DoSomethingWithTheData", ssObjects);
    }




    public void SaveDataOnTheCloud(string UserName, string ModuleName, string QuestionName, int AnswerCount, float CorrectAnswerTime)
    {
        StartCoroutine(SendData(UserName, ModuleName, QuestionName, AnswerCount, CorrectAnswerTime));
    }

    IEnumerator SendData(string UserName, string ModuleName, string QuestionName, int AnswerCount, float CorrectAnswerTime)
    {
        if (!connected)
        {
            Connect();
            yield return new WaitForSeconds(3f);
        }
        string nowTime = System.DateTime.Now.ToString();
        string connectionString = webServiceUrl +
                                    "?ssid=" + spreadsheetId +
                                    "&sheet=" + statisticsWorksheetName +
                                    "&pass=" + password +
                                    "&val1=" + UserName +
                                    "&val2=" + ModuleName +
                                    "&val3=" + QuestionName +
                                    "&val4=" + AnswerCount.ToString() +
                                    "&val5=" + CorrectAnswerTime.ToString() +
                                    "&val6=" + nowTime +
                                    "&action=SetData";

        connectionString = connectionString.Replace(" ", "%20");
        connectionString = connectionString.Replace("Ş", "S");
        connectionString = connectionString.Replace("ş", "s");
        connectionString = connectionString.Replace("İ", "I");
        connectionString = connectionString.Replace("Ğ", "G");
        connectionString = connectionString.Replace("ğ", "g");
        connectionString = connectionString.Replace("Ö", "O");
        connectionString = connectionString.Replace("ö", "o");
        connectionString = connectionString.Replace("Ü", "U");
        connectionString = connectionString.Replace("ü", "u");
        connectionString = connectionString.Replace("Ç", "C");
        connectionString = connectionString.Replace("ç", "c");
        connectionString = connectionString.Replace("ı", "i");



        connectionString = connectionString.Trim();

        if (debugMode)
            Debug.Log("Connection String: " + connectionString);

        WWW www = new WWW(connectionString);
        float elapsedTime = 0.0f;

        while (!www.isDone)
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime >= maxWaitTime)
            {
                // Error handling here.
                Debug.Log("Veriler Yollanmadı  : elapsedTime >= maxWaitTime");
                break;
            }

            yield return null;
        }

        if (!www.isDone || !string.IsNullOrEmpty(www.error))
        {
            // Error handling here.
            Debug.Log("Veriler Yollanmadı  : !www.isDone || !string.IsNullOrEmpty(www.error)   " + www.error);
            yield break;
        }

        string response = www.text;

        if (response.Contains("Incorrect Password"))
        {
            // Error handling here.
            Debug.Log("Veriler Yollanmadı  :   Incorrect Password");
            yield break;
        }

        if (response.Contains("RCVD OK"))
        {

            // Data correctly sent!
            currentStatus = "Veriler Yollandı";
            Debug.Log("Veriler Yollandı ");

            yield break;
        }
    }
}

